module.exports = {
	"env": {
		"browser": true
	},
	"extends": "eslint:recommended",
	"globals": {
		"$": true,
		"ActiveXObject": true,	// Used for IE compatibility
		"AWS": true,
		"Class": true,
		"d3": true,
		"define": true,
		"ga": true,
		"IdanCo": true,
		"jQuery": true,
		"module": true,
		"require": true,
		"Slick": true,
		"sprintf": true,
		"vsprintf": true
	},
	"rules": {
		// "indent": [
		//     "error",
		//     "tab", {
		// 		"SwitchCase": true
		// 	}
		// ],
		"linebreak-style": [
			"error",
			"unix"
		],
		"quotes": [
			"error",
			"single"
		],
		"semi": [
			"error",
			"always"
		],

		// My rules here
		"indent": [ 2, "tab", { "SwitchCase": 1 } ],
		"no-console": 0 // disallow use of console (off by default in the node environment)
	}
};