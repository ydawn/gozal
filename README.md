# GoZal

This HTML5 Game was developed by a dad who wanted to introduce his eight year old son to
programming, and what a better way to do that than developing a game together?

We chose to use JavaScript and HTML5, mostly for the relative ease of development. The
server side is implemented using ExpressJS, without any special changes.
The game itself is a fairly basic 2D shooter, with mostly self made retro graphics.

It took us about two months, on and off, to get to this point; we had many hours of
fun playing against each other (though we called that "QA").

![Sample Game Play](gozal-play.mp4)

While on it, we touched on the following topics, and while we definitely did not become
experts on any of these subjects, we had much fun coming up with solutions to the
problems we encountered:

* Using the excellent PIXI.JS library
* Socket.io for client server communications
* Optimizing network traffic
* Managing frame counting and syncing clients with server
* Using interpolation for client side prediction, to maintain state with an authoritative server.
* Very basic gyro based mobile coding

There are many comments in the code - hopefully making changes to it would not be too
hard for anyone interested. Some of the key subjects are discussed in more detail below. In
any event, don't hesitate to drop me a note in case you need some additional information.

The game was developed on a Mac and an Ubuntu Linux PC, and tested mostly using Chrome. It should
probably work on Windows and other browsers, but it was not tested.

This was definitely a great learning experience :-)

## Getting Started

Get the code from GitHub:

Then run
`npm install`

## Running The Game

The simplest way is to use the default gulp task, just type gulp and the server should start,
serving the game on port 3000.

## How to Play

Control the ship using the arrow keys:

* Arrow up - accelerate
* Arrow down - decelerate
* Arrow right - turn right
* Arrow left - turn left
* Use the space-bar to shoot.

In the two player mode, sharing a keyboard, the second player should use these keys:

* W - accelerate
* S - decelerate
* D - turn right
* A - turn left
* X - fire

The code that controls these assignments is in `Ship2.prototype._onCreate`.

When using a mobile device, tilt the devide in the direction you want to go, for instance,
tilting forward will make it go faster, tilting to the right will make it turn right, etc.
Tap anywhere to shoot. Be patient, it does require some practice :-)

### Changing Direction

The ship will turn faster when it is moving slower, so if you want to make a tight turn,
slow down. Turning while accelerating will result in a wider turn radius.

### Controlling your Velocity

You accelerate using your afterburner. There is a clear visual feedback when that happens.

Acceleration will continue until you reach your maximum speed, or run out of afterburner fuel,
at which point you will slow down gradually to your usual cruising speed. Afterburner fuel
will regenerate with time. The indicator showing your fuel gauge is:

![fuel](assets/img/lightning.png)

You cannot use your afterburner when your ship is badly hit (see below).

You decelerate by turning off your engine. There is a clear visual feedback when that happens.

Deceleration will continue until you reach your minimum speed. Once you stop decelerating, your
engine will automatically turn on, at which point you will gradually speed up to your usual
cruising speed.

### Shooting

You can shoot at any time, as long as your cannon is energized. Any shot will reduce the energy
level of the cannon. The gauge showing the energy level is this:

![energy](assets/img/bullet.png)

Energy level regenerates with time.

### Damage

You will take damage when hit. Your "life" is indicated in this gauge:

![life](assets/img/heart.png)

You regenerate with time. When taking a lot of damage, your ship will get on fire.

When on fire, you will not be able to use your afterburner, regardless of its fuel status.

## Practice Modes

Mostly for the purpose of example, some practice modes are provided.

### Follow that Ship

The goal of this exercise is to follow the computer's ship with yours, maintaining distance
behind it. On screen message will tell you if you are at about the right distance, and what to
do to correct your position in case you are not. In each round, you have to maintain your distance
for five seconds. Once done, you will move to the next level. There are twenty levels, and
your ultimate goal is to complete the mission in the shortest time.

### Target Shooting

This feature does not exist yet.

### Dodge Ball

This feature does not exist yet.

## Debug Modes

Use the Debug modes to check how the game behaves in an interactive way, something that is often
easier to do than running a debugger or sifting through log files. It is especially helpful if you
want to develop your own AI, and pit it against the existing AI. The current algorithm was developed
this way, by making some minor changes and watch the new revision compete against the existing.

__Note: Debug modes are only available when the `debug` flag is on.__

![Sample Debug Play](gozal-debug.mp4)


### Bot vs. Bot

This mode is used to test different algorithms for the AI flying the ship in the Human vs. Computer
mode. To compare the performance of a new algorithm to an existing one, have one of the ships use it,
while the other is using the other. Have them go at it for a few times to establish which one is
superior.

### Server Mock Game

In this mode, the game is simulated as if the opponent is playing using a server, though it does not
shoot. It is useful to see how the movement approximation is working.

### Position

This mode is useful to get a good understanding of the positioning and angle of the ship. In this mode
the ships do not move. You can drag & drop the ships with the mouse to see how the position changes,
and rotate them with the keyboard in the same way they would respond during an actual game. You can
also check the aim and hit testing.

## License

MIT





