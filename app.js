// Packages
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var bodyParser = require('body-parser');
var ejsLayouts = require('express-ejs-layouts');
var nconf = require('nconf');

var app = express();

// Set up config
var config = require('./utils/config')();

// Logger setup
var logger = require('./utils/logger');
app.use(require('morgan')('combined', {
	'stream': logger.stream
}));
logger.info('-----------------------------------');
logger.info('Application start');
logger.info('Environment: ' + nconf.get('NODE_ENV'));
logger.info('-----------------------------------');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());

// Layout
app.use(ejsLayouts);
app.use(express.static(path.join(__dirname, 'public')));

// View engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('layout extractScripts', true); // Put the local scripts at the bottom

// Socket.io
require('./utils/socket-io')(app);

// Routes
app.use('/', require('./routes/phaser'));

// Error handlers
require('./routes/error')(app);

// Phaser
require('./game/phaser')(app);

module.exports = app;