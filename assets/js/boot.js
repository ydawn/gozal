/* global Phaser, game, Modes, DebugModes, Common, W */
var GoZal = {
	// The mode of the game being played
	mode: null,

	// Additional information about the mode
	arg: null,

	// The unique ID of the game (set if this is a server game)
	gameId: null,

	// The running tick time
	gameTime: 0,

	getFormattedGameTime: function() {
		function pad(x) {
			return (x > 10) ? x.toFixed(0) : '0' + x.toFixed(0);
		}

		var totalSec = this.gameTime / 1000;
		if (totalSec < 60) {
			return totalSec.toFixed(1);
		} else {
			var minutes = (totalSec / 60) % 60;
			var seconds = totalSec % 60;
			if (totalSec < 3600) {
				return minutes.toFixed(0) + ':' + pad(seconds);
			} else {
				var hours = totalSec / 3600;
				return hours.toFixed(0) + ':' + pad(minutes) + ':' + pad(seconds);
			}
		}
	},

	isServerGame: function() {
		switch (GoZal.mode) {
			case Modes.MULTI_PLAYER:
				return true;

			case Modes.DEBUG:
				return (GoZal.arg === DebugModes.SERVER_MOCK_GAME || GoZal.arg === DebugModes.SERVER_SHOOTING_RANGE);

			case Modes.SINGLE_PLAYER:
			case Modes.PRACTICE:
			default:
				return false;
		}
	}
};

GoZal.Boot = function(/*game*/) {};

GoZal.Boot.prototype = {

	init: function() {
		// No need for multi-touch
		game.input.maxPointers = 1;

		// Set the scale
		var scale = game.scale;
		var aspectRatio = Common.ASPECT_RATIO;
		scale.setMinMax(480, aspectRatio * 480, W, aspectRatio * W);
		if (game.device.desktop) {
			scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
			scale.pageAlignHorizontally = true;
			// scale.pageAlignVertically = true;
		} else {
			scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
			scale.pageAlignHorizontally = true;
			scale.pageAlignVertically = true;
			scale.forceOrientation(true, false);
			scale.enterIncorrectOrientation.add(this._enterIncorrectOrientation, this);
			scale.leaveIncorrectOrientation.add(this._leaveIncorrectOrientation, this);
		}
		scale.setResizeCallback(this._gameResized, this);
	},

	create: function() {

		// We're going to be using physics, so enable the Arcade Physics system
		game.physics.startSystem(Phaser.Physics.ARCADE);

		// This game will run in Canvas mode, so let's gain a little speed and display
		game.renderer.clearBeforeRender = false;
		game.renderer.roundPixels = true;

		// Calling the load state
		game.state.start('load');
	},

	_gameResized: function(scale, bounds) {
		// This could be handy if you need to do any extra processing if the
		// game resizes.
		// A resize could happen if for example swapping orientation on a
		// device or resizing the browser window.
		// Note that this callback is only really useful if you use a ScaleMode
		// of RESIZE and place it inside your main game state.

		// Align the HTML text elements with the canvas
		$('#text').css('margin-left', (Math.max(0, bounds.width - scale.maxWidth)) * 0.5);
	},

	_enterIncorrectOrientation: function() {
		this.orientated = false;
		$('#orientation').css('display', 'block');
	},

	_leaveIncorrectOrientation: function() {
		this.orientated = true;
		$('#orientation').css('display', 'none');
	}
};