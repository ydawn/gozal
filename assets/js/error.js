/* global GoZal, game, template, GENERIC_ERROR_MESSAGE */

GoZal.Error = function(/*game*/) {};

GoZal.Error.prototype = {

	init: function(error) {
		console.log(error.message);
		this._error = (error && error.message) ? error.message : GENERIC_ERROR_MESSAGE;
	},

	create: function() {
		game.add.tileSprite(0, 0, game.width, game.height, 'starfield');

		var context = {
			title: 'Error',
			message: this._error,
			entries: [{
				text: 'Retry',
				key: 'Esc',
				state: 'menu'
			}]
		};
		var html = template(context);
		$('#text').html(html);
	},

	next: function(state) {
		game.state.start(state, true, false);
	}
};