/* global Phaser, GoZal, W, H */
var game = new Phaser.Game(W, H, Phaser.CANVAS, 'game');

game.state.add('boot', GoZal.Boot);
game.state.add('load', GoZal.Load);
game.state.add('menu', GoZal.Menu);
game.state.add('error', GoZal.Error);
game.state.add('host', GoZal.Host);
game.state.add('join', GoZal.Join);
game.state.add('wait', GoZal.Wait);
game.state.add('ready', GoZal.Ready);
game.state.add('play', GoZal.Play);
game.state.add('over', GoZal.Over);

game.state.start('boot');