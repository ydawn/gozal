/* global GoZal, game, template */
GoZal.Host = function( /*game*/ ) {};

GoZal.Host.prototype = {

	init: function(arg) {
		this._gameId = arg.gameId;
	},

	create: function() {
		console.log('host state created');

		game.add.tileSprite(0, 0, game.width, game.height, 'starfield');

		var context = {
			title: 'Start a Game',
			message: 'Ask the other player to join the game and enter this code:<h2 class="indent">' + this._gameId + '</h2>',
			entries: [{
				text: 'Start Game',
				state: 'wait'
			}, {
				text: 'Cancel',
				key: 'Esc',
				arg: 'cancel'
			}]
		};
		var html = template(context);
		$('#text').html(html);

		// NOTE: clicking Start will not matter much, the game will start when
		// the other player joins...
	},

	next: function(state, mode, arg) {
		if ('cancel' === arg) {
			// Note that at this point the game is already created, so we have
			// to call cancel, not just go back to the menu
			GoZal.socket.cancelGame(GoZal.gameId);
		} else {
			game.state.start(state, true, false);
		}
	}
};