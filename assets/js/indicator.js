/* global Phaser, game */
var Indicator = function(x, y, color, image) {
	this._radius = 30;
	this._value = 1.0;
	this._weight = 0.25;
	this._color = color || '#fff';
	this._image = game.cache.getImage(image);
	this._image.angle = 90;
	this._bmp = game.add.bitmapData((this._radius * 2) + (this._weight * (this._radius * 0.6)), (this._radius * 2) + (this._weight * (this._radius * 0.6)));
	Phaser.Sprite.call(this, game, x, y, this._bmp);

	this.anchor.set(0.5);
	this.angle = -90;
	this._updateDisplay();
};

Indicator.prototype = Object.create(Phaser.Sprite.prototype);
Indicator.prototype.constructor = Indicator;

Indicator.prototype._updateDisplay = function() {
	var value = Phaser.Math.clamp(this._value, 0, 1);

	this._bmp.clear();
	this._bmp.ctx.drawImage(this._image, (this._bmp.width - this._image.width) * 0.5, (this._bmp.height - this._image.height) * 0.5);
	this._bmp.ctx.strokeStyle = this._color;
	this._bmp.ctx.lineWidth = this._weight * this._radius;
	this._bmp.ctx.beginPath();
	this._bmp.ctx.arc(this._bmp.width * 0.5, this._bmp.height * 0.5, this._radius - 15, 0, (Math.PI * 2) * value, false);
	this._bmp.ctx.stroke();
	this._bmp.dirty = true;
};

Indicator.prototype.set = function(value) {
	if (value !== this._value) {
		this._value = value;
		this._updateDisplay();
	}
};