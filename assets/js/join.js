/* global GoZal, game, template, Socket */
GoZal.Join = function(/*game*/) {};

GoZal.Join.prototype = {

	create: function() {
		game.add.tileSprite(0, 0, game.width, game.height, 'starfield');

		var context = {
			title: 'Join a Game',
			message: 'To join a game, enter your code:<div class="indent"><input id="code" type="number" maxlength="4" size="4" min="0" max="99999" step="1"></input></div>',
			entries: [{
				text: 'Join Game',
				state: 'join'
			}, {
				text: 'Cancel',
				key: 'Esc',
				state: 'menu'
			}]
		};
		var html = template(context);
		$('#text').html(html);

		this.$code = $('#code');
		this.$code.focus();

		var self = this;
		this.$code.keypress(function(e) {
			if (e.keyCode === 13) {
				e.preventDefault();
				self._joinGame();
				return false;
			}
		});
	},

	_joinGame: function() {
		var code = this.$code.val();

		// TODO: Do some validation

		// Go to the server now and join the game. The host has already joined
		// the game, so when the guest joins now, the server will emit the
		// game ready event in quick succession and the countdown will start.
		if (!GoZal.socket) {
			GoZal.socket = new Socket(function() {
				GoZal.socket.joinGame(code);
			});
		} else {
			GoZal.socket.joinGame(code);
		}
	},

	next: function(state) {
		if (state === 'join') {
			this._joinGame();
		} else {
			game.state.start(state, true, false);
		}
	}
};