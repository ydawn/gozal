/* global GoZal, game, SHIP_0_COLOR, SHIP_1_COLOR, SHIP_SIZE */

GoZal.Load = function(/*game*/) {};

GoZal.Load.prototype = {

	// The preload function is another standard Phaser function that we
	// use to define and load our assets
	preload: function() {
		game.load.image('starfield', '/images/starfield.jpg');
		game.load.image('fire-1', '/images/fire-1.png');
		game.load.image('fire-2', '/images/fire-2.png');
		game.load.image('fire-3', '/images/fire-3.png');
		game.load.image('smoke', '/images/smoke.png');
		game.load.image('explosion-0', '/images/explosion-0.png');
		game.load.image('explosion-1', '/images/explosion-1.png');
		game.load.image('explosion-2', '/images/explosion-2.png');
		game.load.image('explosion-3', '/images/explosion-3.png');
		game.load.image('explosion-4', '/images/explosion-4.png');
		game.load.image('explosion-5', '/images/explosion-5.png');
		game.load.image('indicator-1', '/images/heart.png');
		game.load.image('indicator-2', '/images/bullet-i.png');
		game.load.image('indicator-3', '/images/lightning.png');

		if (game.device.pixelRatio >= 2) {
			// Rocks sprite created by https://draeton.github.io/stitches/
			game.load.spritesheet('rocks', '/images/rocks@2x.png', 250, 250);
			// Have to find out how to use the retina objects
			game.load.spritesheet('ship0', '/images/ship_' + SHIP_0_COLOR + '.png', SHIP_SIZE, 29);
			game.load.spritesheet('ship1', '/images/ship_' + SHIP_1_COLOR + '.png', SHIP_SIZE, 29);
			game.load.image('bullet', '/images/bullet.png');
		} else {
			game.load.spritesheet('rocks', '/images/rocks.png', 250, 250);
			game.load.spritesheet('ship0', '/images/ship_' + SHIP_0_COLOR + '.png', SHIP_SIZE, 29);
			game.load.spritesheet('ship1', '/images/ship_' + SHIP_1_COLOR + '.png', SHIP_SIZE, 29);
			game.load.image('bullet', '/images/bullet.png');
		}
		game.load.audio('phaser', '/images/phaser.mp3');
		game.load.audio('hit', '/images/hit.mp3');
		game.load.audio('explode', '/images/explode.mp3');

		// Font created using http://kvazars.com/littera/
		//game.load.bitmapFont('arch64y', '/images/arch64y.png', '/images/arch64y.fnt');

		game.time.advancedTiming = true;
	},

	create: function() {
		// Call the menu state
		game.state.start('menu');
	}
};