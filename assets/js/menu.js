/* global GoZal, game, debug, Modes, DebugModes, PracticeModes, PlayLevels, PlayerRoles,
GAME_NAME, template, Socket */

GoZal.Menu = function (/*game*/) {
};

GoZal.Menu._menuTop = [
	{ text: 'Need Practice? Go to Class', state: 'menu', mode: Modes.PRACTICE },
	{ text: 'Lonely? Play the Computer', state: 'menu', mode: Modes.SINGLE_PLAYER },
	{ text: 'Ready? Compete Against a Friend', state: 'menu', mode: Modes.MULTI_PLAYER },
];

if (debug) {
	GoZal.Menu._menuTop.push({ text: 'DEBUG', state: 'menu', mode: Modes.DEBUG });
}

GoZal.Menu._menuDebug = [
	{ text: 'Bot vs. Bot', state: 'play', mode: Modes.DEBUG, arg: DebugModes.BOT_VS_BOT },
	{ text: 'Human vs. Human', state: 'play', mode: Modes.DEBUG, arg: DebugModes.HUMAN_VS_HUMAN },
	{ text: 'Server Shooting Range', key: 'S', state: 'host', mode: Modes.DEBUG, arg: DebugModes.SERVER_SHOOTING_RANGE },
	{ text: 'Server Mock Game', key: 'M', state: 'host', mode: Modes.DEBUG, arg: DebugModes.SERVER_MOCK_GAME },
	{ text: 'Position', state: 'play', mode: Modes.DEBUG, arg: DebugModes.BOT_POSITION },
    { text: 'Back to Menu', key: 'Esc', state: 'menu' }
];

GoZal.Menu._menuPractice = [
	{ text: 'Follow That Ship', state: 'play', mode: Modes.PRACTICE, arg: PracticeModes.FOLLOW },
	{ text: 'Target Shooting', state: 'play', mode: Modes.PRACTICE, arg: PracticeModes.SHOOT },
	{ text: 'Dodge Ball', state: 'play', mode: Modes.PRACTICE, arg: PracticeModes.DODGE },
    { text: 'Back to Menu', key: 'Esc', state: 'menu' }
];

GoZal.Menu._menuOnePlayer = [
	{ text: 'Take Candy from a Baby', state: 'play', mode: Modes.SINGLE_PLAYER, arg: PlayLevels.BEGINNER },
	{ text: 'Fair and Square', state: 'play', mode: Modes.SINGLE_PLAYER, arg: PlayLevels.INTERMEDIATE },
	{ text: 'Death Match', state: 'play', mode: Modes.SINGLE_PLAYER, arg: PlayLevels.EXPERT },
    { text: 'Back to Menu', key: 'Esc', state: 'menu' }
];

GoZal.Menu._menuTwoPlayers = [
	{ text: 'Challenge a Friend to a Duel', state: 'host', mode: Modes.MULTI_PLAYER, arg: PlayerRoles.GAME_HOST },
	{ text: 'Accept a friend\'s Invitation', state: 'join', mode: Modes.MULTI_PLAYER, arg: PlayerRoles.GAME_CLIENT },
	{ text: 'Watch Others Go At It', state: 'view', mode: Modes.MULTI_PLAYER, arg: PlayerRoles.GAME_VIEWER },
    { text: 'Back to Menu', key: 'Esc', state: 'menu' }
];

GoZal.Menu.getMenu = function (mode) {
	switch (mode) {
		case Modes.PRACTICE:
			return GoZal.Menu._menuPractice;
		case Modes.SINGLE_PLAYER:
			return GoZal.Menu._menuOnePlayer;
		case Modes.MULTI_PLAYER:
			return GoZal.Menu._menuTwoPlayers;
		case Modes.DEBUG:
			return GoZal.Menu._menuDebug;
		default:
			return GoZal.Menu._menuTop;
	}
};

GoZal.Menu.getMenuText = function (mode, arg) {
	var menu = GoZal.Menu.getMenu(mode);
	for (var i = 0; i < menu.length; ++i) {
		if (menu[i].arg === arg) {
			return menu[i].text;
		}
	}
	return '';
};

GoZal.Menu.prototype = {

	create: function () {
		// A simple background for our game
		game.add.tileSprite(0, 0, game.width, game.height, 'starfield');
		this._showMenu(GoZal.Menu._menuTop);
	},

	_showMenu: function (menu) {
		var context = {
			title: GAME_NAME,
			message: null,
			entries: menu
		};
		var html = template(context);
		$('#text').html(html);
	},

	next: function (state, mode, arg) {
		GoZal.mode = mode;
		GoZal.arg = arg;

		if (state === 'menu') {
			this._showMenu(GoZal.Menu.getMenu(mode));
		} else if (state === 'play') {
			game.state.start('play', true, false);
		} else if (state === 'host') {
			// Initialize the socket if needed, then ask the socket to
			// create the game. When this is done and confirmed, the
			// state will be set to the "host" state
			if (!GoZal.socket) {
				GoZal.socket = new Socket(function() {
					GoZal.socket.createGame();
				});
			} else {
				GoZal.socket.createGame();
			}
		} else {
			game.state.start(state, true, false);
		}
	}
};
