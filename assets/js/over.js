/* global GoZal, game, template */

GoZal.Over = function(/*game*/) {};

GoZal.Over.prototype = {

	init: function(isWin, message) {
		this._isWin = isWin;
		this._message = message || '';
	},

	create: function() {

		var context = {
			title: 'YOU ' + (this._isWin ? 'WON!' : 'LOST!'),
			message: this._message,
			entries: [{
				text: 'Restart - ' + GoZal.Menu.getMenuText(GoZal.mode, GoZal.arg),
				state: 'play',
				arg: 'restart'
			}, {
				text: 'Back to Menu',
				key: 'Esc',
				state: 'menu'
			}]
		};
		var html = template(context);
		$('#text').html(html);
	},

	_restart: function() {
		$('#text').html('');
		if (GoZal.isServerGame()) {
			GoZal.socket.restartGame(GoZal.gameId);
		} else {
			game.state.start('play', true, false);
		}
	},

	next: function(state, mode, arg) {
		if (arg === 'restart') {
			this._restart();
		} else {
			// Go back to the menu
			game.state.start(state, true, false);
		}
	}
};