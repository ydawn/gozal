/* global game, Phaser, Common, DebugModes, PracticeModes, Modes, GoZal, $, PlayerRoles,
RockGroup, template, gyro, REGENERATE_FREQUENCY, LOG,
Ship, Ship2, BotShip, ServerShip, ServerMockShip, BotFollowShip, BotShipEx, PlayerShip,
DebugShip, DebugShip2 */

var CATCH_UP_HIGH = 2 * Common.CLIENT_FRAME_MS;
var CATCH_UP_LOW = Common.CLIENT_FRAME_MS;

GoZal.Play = function(/*game*/) {};

GoZal.Play.prototype = {

	init: function(arg) {
		this._mode = GoZal.mode;
		this._level = GoZal.arg;
		this._state = arg ? arg.state : Common.initGameState();
		this._countdown = arg ? arg.countdown : Common.COUNT_DOWN;
		this._serverTime = 0;
		this.catchUp = CATCH_UP_HIGH;

		if (game.device.desktop) {
			// No need for gyro
		} else {
			gyro.frequency = 20;
		}
	},

	_createShips: function() {
		// disableVisibilityChange: true to contiune on focus lost;
		// false to pause. Seems to not work on iOS with Chrome maximized.
		var name0 = 'ship0',
			name1 = 'ship1';
		switch (this._mode) {
			case Modes.PRACTICE:
				game.stage.disableVisibilityChange = false;
				this._pauseKey = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
				this._ship0 = new Ship(name0, this._state.ship0);
				switch (this._level) {
					case PracticeModes.FOLLOW:
						this._ship1 = new BotFollowShip(name1, this._state.ship1, this._ship0);
						break;
					case PracticeModes.SHOOT:
						break;
					case PracticeModes.DODGE:
						break;
				}
				break;

			case Modes.SINGLE_PLAYER:
				game.stage.disableVisibilityChange = false;
				this._ship0 = new Ship(name0, this._state.ship0);
				this._ship1 = new BotShipEx(name1, this._state.ship1, this._ship0, this._level);
				break;

			case Modes.MULTI_PLAYER:
				game.stage.disableVisibilityChange = true; // Never pause when network game
				// The game host (the one who created it) always plays ship0.
				if (this._level === PlayerRoles.GAME_HOST) {
					this._ship0 = new PlayerShip(name0, this._state.ship0);
					this._ship1 = new ServerShip(name1, this._state.ship1);
				} else if (this._level === PlayerRoles.GAME_CLIENT) {
					this._ship0 = new ServerShip(name0, this._state.ship0);
					this._ship1 = new PlayerShip(name1, this._state.ship1);
				}
				break;

			case Modes.DEBUG:
				game.stage.disableVisibilityChange = false;
				this._pauseKey = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
				switch (this._level) {
					case DebugModes.BOT_VS_BOT:
						this._ship0 = new BotShipEx(name0, this._state.ship0, null, 3);
						this._ship1 = new BotShip(name1, this._state.ship1, this._ship0, 3);
						this._ship0._ship0 = this._ship1;
						break;
					case DebugModes.HUMAN_VS_HUMAN:
						// On the same client
						this._ship0 = new Ship(name0, this._state.ship0);
						this._ship1 = new Ship2(name1, this._state.ship1);
						break;
					case DebugModes.SERVER_SHOOTING_RANGE:
						this._ship0 = new PlayerShip(name0, this._state.ship0);
						this._ship1 = new ServerShip(name1, this._state.ship1);
						break;
					case DebugModes.SERVER_MOCK_GAME:
						this._ship0 = new PlayerShip(name0, this._state.ship0);
						this._ship1 = new ServerMockShip(name1, this._state.ship1);
						break;
					case DebugModes.BOT_POSITION:
						this._ship0 = new DebugShip(name0, this._state.ship0, null);
						this._ship1 = new DebugShip2(name1, this._state.ship1, this._ship0);
						this._ship0._ship0 = this._ship1;
						break;
				}
				break;

			default:
				console.log('Not implemented yet');
		}

		// For a server game, game over is declared by the server
		if (this._mode !== Modes.MULTI_PLAYER) {
			this._ship0.events.onKilled.add(this._gameOver, this);
			this._ship1.events.onKilled.add(this._gameOver, this);
		}

		game.world.add(this._ship0);
		game.world.add(this._ship1);
	},

	_startCountdown: function() {
		var countdown = this._countdown;
		var context = {
			title: 'Get Ready to Play',
			message: 'The game will start in:<h2 id="count" class="indent">' + countdown + '</h2>',
			entries: [{
				text: 'Pause',
				state: 'pause',
				mode: null
			}, {
				text: 'Cancel',
				key: 'Esc',
				state: 'menu',
				mode: null
			}]
		};
		var html = template(context);
		$('#text').html(html);

		// Do not use a Phaser timer here, the game is paused :-)
		var self = this;
		var interval = setInterval(function() {
			if (--countdown === 0) {
				countdown = 'GO';
			}
			$('#count').text(countdown);
			if (countdown === 'GO') {
				// Countdown ended, we are ready to go.
				// When in server mode, we just wait till the server tells us
				// to start. Otherwise, we start now.
				if (Modes.SINGLE_PLAYER === self._mode ||
					// Modes.MULTI_PLAYER === self._mode ||
					Modes.PRACTICE === self._mode ||
					Modes.DEBUG === self._mode) {
					self._start();
				}
				clearInterval(interval);
			}
		}, 1000);
	},

	_pause: function() {
		game.paused = true;

		var context = {
			title: 'Game Paused',
			message: '',
			entries: [{
				text: 'Resume',
				key: 'Enter',
				state: 'play',
				arg: 'resume'
			}, {
				text: 'Start Over - ' + GoZal.Menu.getMenuText(this._mode, this._level),
				state: 'play',
				arg: 'restart'
			}, {
				text: 'Back to Menu',
				key: 'Esc',
				state: 'menu'
			}]
		};
		var html = template(context);
		$('#text').html(html);
	},

	_start: function() {
		// Un-pause the game.
		$('#text').html('');
		game.paused = false;
		GoZal.gameTime = 0; // Start the accumulator
		this._lastRegenerate = 0;
	},

	_resume: function() {
		$('#text').html('');
		game.paused = false;
	},

	_restart: function() {
		$('#text').html('');
		if (GoZal.isServerGame()) {
			GoZal.socket.restartGame(GoZal.gameId);
		} else {
			game.paused = false;
			game.state.restart(true, false);
		}
	},

	create: function() {
		// A simple background for our game
		game.add.tileSprite(0, 0, game.width, game.height, 'starfield');

		// The countdown to start
		this._startCountdown();

		// Pause the update loop while we are counting down
		game.paused = true;

		// The background rocks
		this._rocksBG = new RockGroup(this._state.rocksBG);

		// The ships
		this._createShips();

		// The foreground rocks
		this._rocksFG = new RockGroup(this._state.rocksFG);
	},

	start: function() {
		this._start();
	},

	update: function() {
		// Update the accumulator
		if (!game.paused) {
			GoZal.gameTime += game.time.physicsElapsedMS;
			// LOG('physicsElapsedMS: ' + game.time.physicsElapsedMS);
			// LOG('GT: ' + GoZal.gameTime + ' delta=' + (Date.now() - GoZal.gameTime));
		}

		if (this._pauseKey && this._pauseKey.isDown) {
			this._pause();
		}

		// Do the hit testing for hits now (it must be at this level, because
		// one ship does not have access to the other's bullets).
		var s0 = this._ship0,
			s1 = this._ship1;

		function hitTest(ship, bullets) {
			// Get the previous location of the ship
			var shipNew = {
				x: ship.x,
				y: ship.y,
				velocity: ship.body.velocity
			};
			Common.interpolatePosition(shipNew, game.time.physicsElapsedMS);

			// For each bullet alive
			bullets.forEachAlive(function(bullet) {
				// Get the old location of the bullet
				var bulletNew = {
					x: bullet.x,
					y: bullet.y,
					velocity: bullet.body.velocity
				};
				Common.interpolatePosition(bulletNew, game.time.physicsElapsedMS);
				// Check if there is a hit
				if (Common.isHit(ship, shipNew, bullet, bulletNew)) {
					// This will handle the ship as needed and will kill or
					// hide the bullet (client or server game, respectively).
					ship.onBeingHit(ship, bullet);
				} else {
					// Make sure the bullet is visible, in case it was hidden
					// before following a client interpolated hit
					bullet.visible = true;
				}
			});
		}

		if (s0.alive && s1.alive) {
			hitTest(s0, s1._bullets);
			hitTest(s1, s0._bullets);
			// game.physics.arcade.overlap(s0, s1, shipsCollide, null, s0);
			if (!GoZal.isServerGame()) {
				if (GoZal.gameTime > this._lastRegenerate + REGENERATE_FREQUENCY) {
					this._regenerate();
					this._lastRegenerate = GoZal.gameTime;
				}
			}
		}
	},

	_gameOver: function() {
		// Go to the game over state. The winner is the one who stayed alive.
		var isWin = this._ship0.alive;
		var message = (Modes.PRACTICE !== this._mode) ? null : 'Total time to complete  ' + GoZal.getFormattedGameTime();
		game.state.start('over', false, false, isWin, message);
	},

	_regenerate: function() {
		this._ship0.doRegenerate();
		this._ship1.doRegenerate();
	},

	_updateShips: function(data) {
		var state = data.state;
		var state0 = state.ship0,
			state1 = state.ship1;
		if (state0.hit) this._ship1.removeBullet(state0.hit.id);
		if (state1.hit) this._ship0.removeBullet(state1.hit.id);
		this._ship0.setState(state0);
		this._ship1.setState(state1);
		var input = data.input;
		this._ship0.setInput(input.ship0);
		this._ship1.setInput(input.ship1);
	},

	_catchUp: function(dt/*, data*/) {
		LOG('catching up; DT: ' + dt);

		// Rocks
		this._rocksBG.move(dt);
		this._rocksFG.move(dt);

		// Bullets
		function moveBullets(bullets) {
			bullets.forEachAlive(function(bullet) {
				var bulletState = {
					x: bullet.x,
					y: bullet.y,
					velocity: bullet.body.velocity
				};
				Common.interpolatePosition(bulletState, dt);
				bullet.x = bulletState.x;
				bullet.y = bulletState.y;
			});
		}
		moveBullets(this._ship0._bullets);
		moveBullets(this._ship1._bullets);

		// this._updateShips(data);

		GoZal.gameTime += dt;

		game.time.events.adjustEvents(GoZal.gameTime);
	},

	onServerUpdate: function(data) {
		var dt = data.gameTime - GoZal.gameTime;
		//LOG('server update; DT: ' + dt + '; Client: ' + GoZal.gameTime + '; Server: ' + data.gameTime);
		// if (dt < -1000) {
		// 	// The server fell far behind, we need to stop
		// 	debugger;
		// }
		this._updateShips(data);
		if (dt <= this.catchUp) {
			if (this.catchUp !== CATCH_UP_HIGH) {
				//LOG('set catch up back to high; GT: ' + GoZal.gameTime + '; UT: ' + data.gameTime);
				this.catchUp = CATCH_UP_HIGH;
			}
		} else {
			if (this.catchUp !== CATCH_UP_LOW) {
				//LOG('set catch up to low; GT: ' + GoZal.gameTime + '; UT: ' + data.gameTime);
				this.catchUp = CATCH_UP_LOW;
			}
			// Quoting Phaser here:
			//  This can happen if the Stage.disableVisibilityChange is true and
			//  you swap tabs, which makes the RequestAnimationFrame pause.
			// Move the client forward in time by dt. If it is farther behind,
			// it might take more than one round to fully catch up. There
			// won't be any updates while doing it.
			this._catchUp(dt, data);
			// this._catchUp(dt + game.time.physicsElapsedMS, data);
			// GoZal.gameTime = data.gameTime + game.time.physicsElapsedMS;
		}

		this._serverTime = data.gameTime;
	},

	onServerGameOver: function(isWin) {
		// This will be initiated by the server, during a server assisted game,
		// when the game is over. In this scenario, we will always have a player
		// ship (the player) going against a server ship (representing the other
		// player). Based on the identity of the winner, we know who should be
		// killed.
		var loser = null;
		if (isWin) {
			// Winner :-)
			loser = (this._ship0.constructor === PlayerShip) ? this._ship1 : this._ship0;
		} else {
			// Loser :-(
			loser = (this._ship0.constructor === PlayerShip) ? this._ship0 : this._ship1;
		}
		loser.doKill();

		// Go to the game over state
		game.state.start('over', false, false, isWin);
	},

	next: function(state, mode, arg) {
		if (state === 'play') {
			if (arg === 'resume') {
				this._resume();
			} else if (arg === 'restart') {
				this._restart();
			}
		} else {
			if (game.paused) {
				game.paused = false;
			}
			if (GoZal.isServerGame()) {
				GoZal.socket.cancelGame(GoZal.gameId);
			}
			game.state.start(state, true, false);
		}
	},

	render: function() {
		// if (debug) {
		// 	game.debug.text('GT: ' + (GoZal.gameTime.toFixed(0) || '0') + ', ST: ' + (this._serverTime.toFixed(0) || '0') + ', DT: ' + ((GoZal.gameTime.toFixed(0) - this._serverTime.toFixed(0)) || '0'), 2, 28, "#00ff00");
		// 	game.debug.text(game.time.fps || '--', 2, 14, '#00ff00');
		// 	game.debug.spriteInfo(this._ship0, 32, 32);
		// 	game.debug.body(this._ship0);
		// 	game.debug.body(this._ship1);
		// }
	}
};