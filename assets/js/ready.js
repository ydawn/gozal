/* global Phaser, GoZal, game, template */
GoZal.Ready = function(/*game*/) {};

GoZal.Ready.prototype = {

	init: function(arg) {
		this._countdown = arg.countdown;
	},

	create: function() {
		game.add.tileSprite(0, 0, game.width, game.height, 'starfield');

		var context = {
			title: 'Get Ready to Play',
			message: 'The game will start in:<h2 id="count" class="indent">' + this._countdown + '</h2>',
			entries: [{
				text: 'Pause',
				state: 'pause'
			}, {
				text: 'Cancel',
				key: 'Esc',
				state: 'menu'
			}]
		};
		var html = template(context);
		$('#text').html(html);

		game.time.events.repeat(Phaser.Timer.SECOND, this._countdown, this._updateCountdown, this);
	},

	_updateCountdown: function() {
		if (--this._countdown === 0) {
			this._countdown = 'GO';
		}
		$('#count').text(this._countdown);
		if (this._countdown === 'GO') {
			this.next('play');
		}
	},

	_pauseGame: function() {},

	next: function(state) {
		if (state === 'pause') {
			this._pauseGame();
		} else {
			game.state.start(state, true, false);
		}
	}
};