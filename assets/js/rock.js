/* global Phaser, W, game, Common */
var Rock = function(options) {
	Phaser.Sprite.call(this, game, options.x, options.y, 'rocks', options.imageIndex);
	this.anchor.setTo(0.5, 0.5);
	this.scale.setTo(options.scale);
	this.velocity = options.velocity;
	this._w_2 = this.width * 0.5;
	this._lastX = options.x;
};

Rock.prototype = Object.create(Phaser.Sprite.prototype);
Rock.prototype.constructor = Rock;

Rock.prototype.move = function(dt) {
	this._lastX = Common.wrap(this._lastX + this.velocity.x * dt, -this._w_2, W + this._w_2);
	if (Math.round(this._lastX) !== this.position.x) {
		this.position.x = Math.round(this._lastX);
	}
};

Rock.prototype.update = function() {
	// There is no physics body for the rocks, so we have to simulate movement
	this.move(this.game.time.physicsElapsedMS);
};

var RockGroup = function(rockData) {
	this._rocks = game.add.group();
	for (var i = 0; i < rockData.length; ++i) {
		this._rocks.add(new Rock(rockData[i]));
	}
};

RockGroup.prototype.move = function(dt) {
	this._rocks.forEach(function(rock) {
		rock.move(dt);
	});
};