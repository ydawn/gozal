/* global Phaser, W, game, Common, SHIP_SIZE, SHIP_SIZE_2, H, W_2, H_2, H_3, PI_1_2, SIDELINE,
SHOOT_RANGE_LONG, SHOOT_RANGE_SHORT, SHIP_0_COLOR, SHIP_1_COLOR, FOLLOW_MIN_TIME, FOLLOW_DIST2_GOOD, FOLLOW_DIST2_TOO_CLOSE,
PlayLevels, $, GoZal, gyro, Common, Phaser, Indicator, game, debug */
var SHIP_DATA = Common.SHIP_DATA;
var ShipState = Common.ShipState;

var Ship = function(name, options) {
	this._name = name;

	// Sprite
	Phaser.Sprite.call(this, game, options.x, options.y, this._name);
	this.anchor.setTo(0.5, 0.5);
	this.rotation = options.rotation;

	// Our two animations, with and without afterburner.
	this.animations.add('normal', [1, 2], 10, true);
	this.animations.add('fast', [3, 4], 10, true);

	// Body
	game.physics.arcade.enable(this);
	var body = this.body;
	body.width = SHIP_SIZE;
	body.height = SHIP_SIZE;
	body.collideWorldBounds = false;
	body.gravity.y = 0;
	body.velocity = options.velocity;
	body.maxVelocity.set(SHIP_DATA.maxSpeed);

	// Sounds
	this._phaser = game.add.audio('phaser');
	this._hit = game.add.audio('hit');

	// Bullets
	var bullets = game.add.group();
	bullets.enableBody = true;
	bullets.physicsBodyType = Phaser.Physics.ARCADE;
	bullets.createMultiple(SHIP_DATA.bulletLimit, 'bullet');
	bullets.setAll('checkWorldBounds', true);
	bullets.setAll('outOfBoundsKill', true);
	if (GoZal.isServerGame()) {
		bullets.setAll('id', null);
		bullets.callAll('events.onKilled.add', 'events.onKilled', function(bullet) {
			// console.log('reset killed bullet ' + bullet.id);
			bullet.id = null;
		}, this);
	}
	this._bullets = bullets;
	this._nextFire = options.nextFire;

	// Particle system
	this._emitter = game.add.emitter(game.world.centerX, game.world.centerY, 20);
	this._emitter.makeParticles(['fire-1', 'fire-2', 'fire-3', 'smoke', 'smoke'], 0);
	this._emitter.gravity = 0;
	this._emitterDie = game.add.emitter(game.world.centerX, game.world.centerY, 20);

	// State
	this._isOnFire = options.isOnFire;
	this._isAfterBurner = options.isAfterBurner;
	this._isBreakOn = options.isBreakOn;
	this._hitPoints = options.hitPoints;
	this._charge = options.charge;
	this._energy = options.energy;
	// this._damper = 0;

	// Indicators
	this._createIndicators();

	// Let the caller do its thing now
	this._onCreate();
};

Ship.prototype = Object.create(Phaser.Sprite.prototype);
Ship.prototype.constructor = Ship;

Ship.prototype._createIndicators = function() {
	var ix0, ix1, ix2, icolor;
	if (this._name === 'ship0') {
		ix0 = W - 30;
		ix1 = W - 70;
		ix2 = W - 110;
		icolor = SHIP_0_COLOR;
	} else {
		ix0 = 30;
		ix1 = 70;
		ix2 = 110;
		icolor = SHIP_1_COLOR;
	}

	function createIndicator(ix, image) {
		var indicator = new Indicator(ix, H - 30, icolor, image);
		game.world.add(indicator);
		return indicator;
	}

	this._indicatorHitPoints = createIndicator(ix0, 'indicator-1');
	this._indicatorCharge = createIndicator(ix1, 'indicator-2');
	this._indicatorEnergy = createIndicator(ix2, 'indicator-3');
};

Ship.prototype._onCreate = function() {
	// The default controls
	this._fastKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
	this._slowKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
	this._leftKey = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
	this._rightKey = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
	this._fireKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

	if (game.device.desktop) {
		// No need for mobile controls
	} else {
		this._isMobile = true;
		this._direction = this._accelerate = 0;
		var self = this;
		gyro.startTracking(function(o) {
			// $('#log').html('Gamma: ' + o.gamma);
			if (o.beta < -10) {
				self._direction = -1;
			} else if (o.beta > 10) {
				self._direction = 1;
			} else {
				self._direction = 0;
			}
			if (o.gamma < -40) {
				self._accelerate = -1;
			} else if (o.gamma > -20) {
				self._accelerate = 1;
			} else {
				self._accelerate = 0;
			}
		});
	}

	if (debug) {
		this.inputEnabled = true;
		this.input.enableDrag(true);
	}
};

Ship.prototype._getInput = function() {
	// if (game.input.activePointer.isDown) debugger;
	if (!this.alive) {
		return {};
	}
	// if (++this._damper === Common.INPUT_FREQUENCY) {
	//     this._damper = 0; // Reset and let go through
	// } else {
	//     return {};
	// }

	return {
		goFaster: this._fastKey.isDown || (this._accelerate === 1) || false,
		goSlower: this._slowKey.isDown || (this._accelerate === -1) || false,
		goRight: this._rightKey.isDown || (this._direction === 1) || false,
		goLeft: this._leftKey.isDown || (this._direction === -1) || false,
		doShoot: this._fireKey.isDown || (this._isMobile && game.input.activePointer.isDown) || false,
		gameTime: GoZal.gameTime
	};
};

Ship.prototype._getState = function() {
	return new ShipState(
		GoZal.gameTime,
		this.x, this.y, this.body.velocity, this.rotation,
		this._hitPoints, this._charge, this._energy, this._nextFire,
		this._isOnFire, this._isAfterBurner, this._isBreakOn);
};

Ship.prototype._filterInput = function(input) {
	return Common.filterInput(input, this._getState());
};

Ship.prototype._getNewState = function(input) {
	return Common.processInput(this._getState(), input);
};

Ship.prototype._setState = function(state) {
	// It looks like with Phasor you can control velocity and rotation on the
	// body, or rotation on the sprite and velocity on the body (using the
	// sin-cos directly). We do the latter.
	this.rotation = state.rotation;
	this.body.velocity = state.velocity;

	// Attributes of the ship
	this._nextFire = state.nextFire;
	this._isAfterBurner = state.isAfterBurner;
	this._isBreakOn = state.isBreakOn;
	this._setOnFire(state.isOnFire);
	this._setEnergy(state.energy);
	this._setCharge(state.charge);

	// Shooting
	if (state.shoot) {
		var sd = state.shoot.bullet;

		// Show on screen, after interpolating as needed. It won't do anything
		// for the single player mode, will only adjust for the server update.
		Common.interpolatePosition(sd, GoZal.gameTime - state.gameTime);
		var bullet = this._bullets.getFirstDead();
		bullet.anchor.setTo(0, 0.5);
		bullet.reset(sd.x, sd.y);
		bullet.body.velocity.x = sd.velocity.x;
		bullet.body.velocity.y = sd.velocity.y;
		bullet.rotation = sd.rotation;
		bullet.enableBody = true;
		bullet.id = sd.id; // A value will exist only when there is a server
		this._phaser.play();
	}
};

Ship.prototype._updateDisplay = function() {
	if (!this.alive) return;

	if (this._isAfterBurner) {
		// Accelerate
		this.animations.play('fast');
	} else if (this._isBreakOn) {
		// Break
		this.frame = 0;
	} else {
		// Cruise
		this.animations.play('normal');
	}

	// If we are on fire, show it
	if (this._isOnFire || this._isBlasted) { // Get the back of the ship
		var body = this.body;
		var px = -body.velocity.x * Math.random(),
			py = -body.velocity.y * Math.random();
		var cos = Math.cos(this.rotation),
			sin = Math.sin(this.rotation);
		this._emitter.minParticleSpeed.set(px, py);
		this._emitter.maxParticleSpeed.set(px, py);
		this._emitter.emitX = this.x - SHIP_SIZE_2 * cos;
		this._emitter.emitY = this.y - SHIP_SIZE_2 * sin;
		if (!this._emitter.on) {
			this._emitter.flow(250, 4);
			this._emitter.setAlpha(1, 0.25, 500);
			this._emitter.setScale(0.8, 1.6, 0.8, 1.6);
		}
	}
};

Ship.prototype._onUpdate = function() {};

Ship.prototype.update = function() {
	if (!this.alive) return;

	// Screen wrap-around
	this.x = Common.wrap(this.x, -SHIP_SIZE_2, W + SHIP_SIZE_2);
	this.y = Common.wrap(this.y, -SHIP_SIZE_2, H + SHIP_SIZE_2);

	// Get the input now and handle it
	var input = this._filterInput(this._getInput());

	// This is the prediction code - it gets the new state from the current
	// one, in the hopes it would match closely whatever we get from the
	// server later. Note that it only takes the input here - the working
	// assumption is that all state and timing info is accurate.
	var state = this._getNewState(input);
	this._setState(state);

	// Update the display as needed
	this._updateDisplay();

	// Let the caller do its thing now
	this._onUpdate();
};

Ship.prototype.doRegenerate = function() {
	if (!this.alive) return;

	var newState = Common.onRegenerate(this._getState());
	this._setHitPoints(newState.hitPoints);
	this._setCharge(newState.charge);
	this._setEnergy(newState.energy);
	this._setOnFire(newState.isOnFire);
};

Ship.prototype._showBlast = function() {
	this._isBlasted = true;
	var self = this;
	setTimeout(function() {
		self._isBlasted = false;
		if (!this._isOnFire) {
			self._emitter.on = false;
		}
	}, 100);
};

Ship.prototype._onHit = function() {
	if (!this.alive) return;

	// Handle UI for the hit
	if (0 < this._hitPoints) {
		// This is a hit, but not a fatal wound
		this._hit.play();
		if (!this._isOnFire) {
			// When hit but not on fire, give a little burst of flames
			this._showBlast();
		}
	} else {
		// We are done for
		this._onDie();
	}
};

Ship.prototype._onDie = function() {
	// Stop the current emitter
	this._isOnFire = false;
	this._emitter.on = false;

	// Prepare the emitter for the last hurrah
	this._emitterDie.makeParticles(['explosion-0', 'explosion-1', 'explosion-2', 'explosion-3', 'explosion-4', 'explosion-5'], 0);
	this._emitterDie.setScale(1, 0, 1, 0, 1500);
	this._emitterDie.emitX = this.x;
	this._emitterDie.emitY = this.y;
	this._emitterDie.gravity = 0;

	// Ready to go, explode it
	var explode = game.add.audio('explode');
	explode.play();

	// The first parameter sets the effect to "explode" which means all particles
	// are emitted at once.
	// The second gives each particle a 3500ms lifespan
	// The third is ignored when using burst/explode mode
	// The final parameter (20) is how many particles will be emitted in this
	// single burst
	this._emitterDie.start(true, 3500, null, 20);

	// Signal the death
	this.kill();

	// Reclaim the memory
	this._indicatorEnergy.kill();
	this._indicatorCharge.kill();
	this._indicatorHitPoints.kill();
};

Ship.prototype.removeBullet = function(id) {
	var bullets = this._bullets.filter(function(bullet/*, index, bullets*/) {
		return bullet.id === id;
	}, true);
	if (bullets.total > 0) {
		var hitBullet = bullets.first;
		hitBullet.id = 'hit'; // Handled in the onKilled event as well
		hitBullet.kill();
	}
};

Ship.prototype._setHitPoints = function(value) {
	if (value === this._hitPoints) return;
	this._hitPoints = Math.min(value, SHIP_DATA.maxHitPoints); // *Can* go negative => death
	this._indicatorHitPoints.set(this._hitPoints / SHIP_DATA.maxHitPoints);
};

Ship.prototype._setCharge = function(value) {
	if (value === this._charge) return;
	this._charge = Common.clamp(value, 0, SHIP_DATA.maxCannonCharge);
	this._indicatorCharge.set(this._charge / SHIP_DATA.maxCannonCharge);
};

Ship.prototype._setEnergy = function(value) {
	if (value === this._energy) return;
	this._energy = Common.clamp(value, 0, SHIP_DATA.maxEnergy);
	this._indicatorEnergy.set(this._energy / SHIP_DATA.maxEnergy);
	if (this._energy <= 0) {
		this._isAfterBurner = false;
	}
};

Ship.prototype._setOnFire = function(isOnFire) {
	if (this._isOnFire && !isOnFire) {
		// Bad damage fixed, yey
		this._emitter.on = false;
	}
	this._isOnFire = isOnFire;
};

Ship.prototype.onBeingHit = function(ship, bullet) {
	// Ship is hit by a bullet
	var newState = Common.onHit(this._getState(), {
		velocity: bullet.body.velocity,
		rotation: bullet.rotation
	});
	this.rotation = newState.rotation;
	this.body.velocity = newState.velocity;
	this._setHitPoints(newState.hitPoints);
	this._isOnFire = newState.isOnFire;

	ship._onHit();
	bullet.kill();
};

// This function is disabled, it appears to be killing the playability
// Ship.prototype.onCollide = function() {
//     // For now, die
//     this._setHitPoints(0);
// };

// Inheritance Phaser style
// Ship2 is the second ship when playing on the same PC (this is mostly a debug
// mode, it probably won't be used once we have remote play).
var Ship2 = function(name, options) {
	Ship.call(this, name, options);
};

Ship2.prototype = Object.create(Ship.prototype);
Ship2.prototype.constructor = Ship2;

Ship2.prototype._onCreate = function() {
	// Use different controls than the default so the other player
	// can control her ship :-)
	this._fastKey = game.input.keyboard.addKey(Phaser.Keyboard.W);
	this._slowKey = game.input.keyboard.addKey(Phaser.Keyboard.S);
	this._leftKey = game.input.keyboard.addKey(Phaser.Keyboard.A);
	this._rightKey = game.input.keyboard.addKey(Phaser.Keyboard.D);
	this._fireKey = game.input.keyboard.addKey(Phaser.Keyboard.X);
};

// BotFollowShip is the second ship when practicing following.
var BotFollowShip = function(name, options, ship0) {
	this._ship0 = ship0;

	// Always start in the middle, going right
	options.x = W_2;
	options.y = H_3;
	options.rotation = 0;
	options.velocity.x = Common.SHIP_DATA.cruiseSpeed;

	// Set up the parameters for the path.
	// "turn" controls how tight it is (higher makes for bigger waves)
	// "speed" controls the angular change (lower is faster)
	// Level 2N is the same as level N, but with acceleration.
	this._TRAIL_DATA = [{
		// Just a straight line
		turn: 0,
		speed: 0
	}, {
		turn: 30,
		speed: 7
	}, {
		turn: 30,
		speed: 5
	}, {
		turn: 30,
		speed: 3
	}, {
		turn: 45,
		speed: 7
	}, {
		turn: 45,
		speed: 5
	}, {
		turn: 45,
		speed: 3
	}, {
		turn: 60,
		speed: 7
	}, {
		turn: 60,
		speed: 5
	}, {
		turn: 60,
		speed: 3
	}];
	this._level = -1;
	this._increaseLevel(); // Will set it to 0 to start it all

	// Use the text element to provide feedback
	this._$text = $('#text');

	// Call the base to create the ship
	Ship.call(this, name, options);
};

// Inheritance Phaser style
BotFollowShip.prototype = Object.create(Ship.prototype);
BotFollowShip.prototype.constructor = BotFollowShip;

BotFollowShip.prototype._onCreate = function() {
	// Set up the emitter that displayes the trail
	this._emitterFollow = game.add.emitter(game.world.centerX, game.world.centerY, 100);
	this._emitterFollow.makeParticles(['fire-1'], 0);
	this._emitterFollow.gravity = 0;
	this._emitterFollow.minParticleSpeed.set(0, 0);
	this._emitterFollow.maxParticleSpeed.set(0, 0);
	this._emitterFollow.start(false, 5000, 100);
	this._emitterFollow.setAlpha(1, 0.05, 5000);
};

BotFollowShip.prototype._onDie = function() {
	// Stop the emitter
	this._emitterFollow.on = false;

	// Let the base complete
	Ship.prototype._onDie.call(this);
};

BotFollowShip.prototype._increaseLevel = function() {
	var l = ++this._level,
		len = this._TRAIL_DATA.length;
	if (l >= 2 * len) {
		// We are done
		this._onDie();
		return;
	}

	// Set up the trail paramateres for the next level
	this._trail = this._TRAIL_DATA[(l < len) ? l : (l - len)];
	this._trail.direction = 1;
	this._trail.acceleration = (l >= len);
	this._trail.tick = 0;
	this._trail.timeStart = null; // The beginning of the period in compliance

	function onComplete() {
		this._emitterFollow.on = true;
		this.body.velocity.x = Common.SHIP_DATA.cruiseSpeed;
		this._inLevelTransition = false;
	}

	// The ship has to return to center before we can proceed.
	if (this._level >= 1) {
		this._inLevelTransition = true;
		var tween = game.add.tween(this);
		tween.onStart.add(function() {
			this._emitterFollow.on = false;
			this._emitterFollow.forEachExists(function(element) {
				element.kill();
			}, this);
		}, this);
		if (this.x < W_2) {
			// Left side of the screen - simple tween
			tween.to({
				x: W_2,
				y: H_3,
				angle: 0
			}, 2000 * (this.x / W_2), Phaser.Easing.Quintic.InOut, true);
			tween.onComplete.add(onComplete, this);
		} else {
			// Right side of the screen - two steps process
			tween.to({
				x: W + SHIP_SIZE,
				y: H_3,
				angle: 0
			}, 1000 * ((W - this.x) / W_2), Phaser.Easing.Quintic.In, true);
			tween.onComplete.add(function() {
				this.x = -SHIP_SIZE;
				var tween2 = game.add.tween(this).to({
					x: W_2,
					y: H_3,
					angle: 0
				}, 1000, Phaser.Easing.Quintic.Out, true);
				tween2.onComplete.add(onComplete, this);
			}, this);
		}
	}
};

BotFollowShip.prototype._updateDisplay = function() {
	Ship.prototype._updateDisplay.call(this);

	var cos = Math.cos(this.rotation),
		sin = Math.sin(this.rotation);
	this._emitterFollow.emitX = this.x - 10 * cos;
	this._emitterFollow.emitY = this.y - 10 * sin;
};

BotFollowShip.prototype._getInput = function() {
	// Do not take client input

	// The trail to follow is defined by the level.
	if (0 === this._level) {
		return {}; // This will result in a straight line.
	}

	var input = {};
	var self = this;

	function doSnakeLine() {
		var t = self._trail;
		++t.tick;

		// Start with the speed of the ship (only if needed)
		if (t.acceleration) {
			// 19 - 49: go fast
			// 69 - 99: go slow
			// else: no change
			var a = t.tick % 100;
			if (19 <= a && a <= 49) {
				input.goFaster = true;
			} else if (69 <= a && a <= 99) {
				input.goSlower = true;
			}
		}

		// Change direction based on the speed of the trail
		if (t.tick % t.speed > 1) return;
		var r = self.body.rotation;
		if (t.direction === 1) {
			if (r < t.turn) {
				input.goRight = true;
			} else {
				input.goLeft = true;
				t.direction = -1;
			}
		} else {
			if (r > -t.turn) {
				input.goLeft = true;
			} else {
				input.goRight = true;
				t.direction = 1;
			}
		}
	}

	doSnakeLine();

	return input;
};

BotFollowShip.prototype.onBeingHit = function(ship, bullet) {
	// Ship is hit by a bullet, but it is immune.
	// Show the hit, but nothing else
	ship._onHit();
	bullet.kill();
};

BotFollowShip.prototype._giveFeedback = function(status, isGood) {
	var timeToNextLevelMsec = FOLLOW_MIN_TIME - ((null === this._trail.timeStart) ? 0 : (GoZal.gameTime - this._trail.timeStart));
	var timeToNextLevel = (timeToNextLevelMsec / 1000).toFixed(1);
	// The displayed level is one-based.
	var level = 'Level: ' + (this._level + 1) + ' - time to next level: ' + timeToNextLevel + 's';
	var totalTime = 'Time: ' + GoZal.getFormattedGameTime();
	this._$text.html('<div class="practice-feedback"><div class="subtext">' + level +
		'</div><div class="subtext">' + totalTime +
		'</div><h3 class="text ' + (isGood ? 'good' : 'bad') + '">' + status + '</h3></div>');
};

BotFollowShip.prototype._onUpdate = function() {
	if (!this.alive || this._inLevelTransition || game.paused) return;

	// Grade the player by how far she is from the trail
	// Find the minimal distance accross all of them that are bright
	// enough (gives a fair approximation of the distance)
	var minDistance = W;
	this._emitterFollow.forEachExists(function(element) {
		if (element.alpha > 0.5) {
			minDistance = Math.min(minDistance, Common.getDistance2(this._ship0, element));
		}
	}, this);

	if (minDistance < FOLLOW_DIST2_GOOD) {
		if (Common.getDistance2(this._ship0, this) < FOLLOW_DIST2_TOO_CLOSE) {
			// Too close to the other ship - fail
			this._trail.timeStart = null;
			this._giveFeedback('Too close - stay back', false);
		} else {
			// On path - success
			if (null === this._trail.timeStart) {
				// Set the start time
				this._trail.timeStart = GoZal.gameTime;
			} else {
				// Check if we qualify
				if (GoZal.gameTime - this._trail.timeStart > FOLLOW_MIN_TIME) {
					// Go to the next level
					this._giveFeedback('Level completed', true);
					this._increaseLevel();
					return;
				}
			}
			this._giveFeedback('On track - keep going', true);
		}
	} else {
		// Too far from the other ship - fail
		this._trail.timeStart = null;
		this._giveFeedback('Too far - get closer', false);
	}
};

// Inheritance Phaser style
// BotShip is the second ship when playing against the computer.
var BotShip = function(name, options, ship0, level) {
	this._level = level;
	this._ship0 = ship0;

	Ship.call(this, name, options);
};

BotShip.prototype = Object.create(Ship.prototype);
BotShip.prototype.constructor = BotShip;

// Replace the implementation of the base
BotShip.prototype._onCreate = function() {
	if (PlayLevels.EXPERT === this._level) {
		// Full power -> no reduction
		// BotShip.prototype._adjustInput = function (input) {
		this._adjustInput = function(input) {
			return input;
		};
	} else {
		// Reduce the bot performance by this ratio
		var r = (PlayLevels.BEGINNER === this._level) ? 2 : 5;
		var i = 0;
		// BotShip.prototype._adjustInput = function (input) {
		this._adjustInput = function(input) {
			if (++i % r === 0) {
				input.goRight = input.goLeft = input.doShoot = false;
			}
			return input;
		};
	}
};

// The main AI routine. To keep things fair, the computer is limited to the
// same interface used by the player, meaning that it provides input which is
// then processed in the same manner the player's input is handled.
BotShip.prototype._getInput = function() {
	// ship0 is always the player. ship1 is the bot.
	var s0 = this._ship0,
		s1 = this;
	if (!s0 || !s0.alive || !s1.alive) return {}; // This is done

	var input = {};

	// We will get a better result if we interpolate the location of the other
	// ship for the purpose of shooting. To simplify the computation, use a
	// factor to account for the distance approximation due to the triangle
	// created by the location of this ship, the other and the other's position
	// after interpolation. Setting it to be smaller than one makes sense as this
	// more likely would be the case the bot would actually take a shot.
	var s0i = {
		x: s0.x,
		y: s0.y,
		velocity: s0.body.velocity,
		rotation: s0.rotation
	};
	var actualDistance = 0.6 * game.physics.arcade.distanceBetween(s1, s0i);
	var dt = 1000 * actualDistance / Common.SHIP_DATA.bulletSpeed;
	Common.interpolatePosition(s0i, dt);

	// If we are far, or the interpolation goes out of bounds, sit this round out...
	var noShoot = (distance > SHOOT_RANGE_LONG) ||
		(s0i.x < -SHIP_SIZE_2 || s0i.x > W + SHIP_SIZE_2 ||
			s0i.y < -SHIP_SIZE_2 || s0i.y > H + SHIP_SIZE_2);

	// We want to shoot when the other ship is in front of us and in close range.
	// If it is far, we only shoot if the trajectories are aligned.
	// If the angle to the other ship is not good for shooting, we want to rotate.

	// Work the sidelines. If we are close to the side and with our back to the
	// center, we want to warp (wrap) - this is the fastest way to have the
	// cannon facing the action.
	var rotation = s1.rotation;
	if (s1.x < SIDELINE) {
		// Left
		if (rotation < -PI_1_2) {
			input.goLeft = true;
		} else if (rotation > PI_1_2) {
			input.goRight = true;
		}
	} else if (s1.x > W - SIDELINE) {
		// Right
		if (0 > rotation && rotation > -PI_1_2) {
			input.goRight = true;
		} else if (PI_1_2 > rotation && rotation > 0) {
			input.goLeft = true;
		}
	} else if (s1.y < SIDELINE) {
		// Top
		if (-PI_1_2 > rotation) {
			input.goRight = true;
		} else if (0 > rotation && rotation > -PI_1_2) {
			input.goLeft = true;
		}
	} else if (s1.y > H - SIDELINE) {
		// Bottom
		if (PI_1_2 > rotation && rotation > 0) {
			input.goRight = true;
		} else if (rotation > PI_1_2) {
			input.goLeft = true;
		}
	}
	if (input.goRight || input.goLeft) {
		// If we make a move towards the sideline, get it done quickly
		input.goFaster = true;
		return input;
	}

	// Get the location info for both ships
	var cos = Math.cos(rotation),
		sin = Math.sin(rotation);
	var s1t = {
		x: s1.x + SHIP_SIZE_2 * cos,
		y: s1.y + SHIP_SIZE_2 * sin
	};
	var distance = game.physics.arcade.distanceBetween(s1t, s0i);
	var angle = game.physics.arcade.angleBetween(s1, s0i);
	var dAngle = Phaser.Math.wrapAngle(rotation - angle, true);
	var absdAngle = Math.abs(dAngle);

	// Shoot
	var angularSize = Math.atan(SHIP_SIZE_2 / distance);
	if (!noShoot &&
		((distance > 2 * SHIP_SIZE && Phaser.Math.within(dAngle, 0, angularSize)) ||
			(Phaser.Math.within(dAngle, 0, 0.5 * angularSize)))) {
		// If we are not too far and the angle is narrow enough, shoot
		input.doShoot = true;
	}

	// Steer, try to get the other ship in-front
	if (0 < dAngle) {
		// Left
		input.goLeft = true;
	} else {
		// Right
		input.goRight = true;
	}

	// Handle the speed
	if (input.doShoot) {
		// If we shoot, based on the distance, decide if we have to close the gap
		if (distance < SHOOT_RANGE_SHORT) {
			input.goSlower = true;
		} else if (distance > SHOOT_RANGE_LONG) {
			input.goFaster = true;
		}
	} else if (distance < SHOOT_RANGE_LONG && Phaser.Math.within(absdAngle, Math.PI, angularSize)) {
		// Other ship behind us and close, have to run away fast
		input.goFaster = true;
	} else if (distance > SHOOT_RANGE_LONG) {
		// Other ship is out of range, close the gap quickly.
		// A side effect if doing it here is that when the ships are far
		// and we are steering, the ship would follow a wide arc shape -
		// instead of a tight turn. This is probably better (and sure is
		// when tested head to head) as the ship is not a sitting duck.
		input.goFaster = true;
		// } else if ((distance > SHOOT_RANGE_SHORT) && (distance < SHOOT_RANGE_LONG) &&
		// 	(Phaser.Math.within(dAngle, Math.PI / 2, angularSize) ||
		// 	Phaser.Math.within(dAngle, -Math.PI / 2, angularSize))) {
		// 	// Other ship close on our side, run away fast
		// 	input.goFaster = true;
	} else {
		if (absdAngle > Math.PI / 4) {
			// If we have a lot of angular distance to cover, slow down
			// for a tighter turn, unless we are very close, in which
			// case we want to break contact
			if (distance < SHIP_SIZE) {
				input.goFaster = true;
			} else {
				input.goSlower = true;
			}
		}
	}

	// Do some adjustment based on the level
	input = this._adjustInput(input);

	input.gameTime = GoZal.gameTime;
	return input;
};

// Inheritance Phaser style
// This is the experimental bot, that will compete against the other bot.
var BotShipEx = function(name, options, ship0, level) {
	BotShip.call(this, name, options, ship0, level);
};

BotShipEx.prototype = Object.create(BotShip.prototype);
BotShipEx.prototype.constructor = BotShipEx;

BotShipEx.prototype._getInput = function() {
	// ship0 is always the player. ship1 is the bot.
	var s0 = this._ship0,
		s1 = this;
	if (!s0 || !s0.alive || !s1.alive) return {}; // This is done

	var input = {};

	// We will get a better result if we interpolate the location of the other
	// ship for the purpose of shooting. To simplify the computation, use a
	// factor to account for the distance approximation due to the triangle
	// created by the location of this ship, the other and the other's position
	// after interpolation. Setting it to be smaller than one makes sense as this
	// more likely would be the case the bot would actually take a shot.
	var s0i = {
		x: s0.x,
		y: s0.y,
		velocity: s0.body.velocity,
		rotation: s0.rotation
	};
	var actualDistance = 0.6 * game.physics.arcade.distanceBetween(s1, s0i);
	var dt = 1000 * actualDistance / Common.SHIP_DATA.bulletSpeed;
	Common.interpolatePosition(s0i, dt);

	// If we are far, or the interpolation goes out of bounds, sit this round out...
	var noShoot = (distance > SHOOT_RANGE_LONG) ||
		(s0i.x < -SHIP_SIZE_2 || s0i.x > W + SHIP_SIZE_2 ||
			s0i.y < -SHIP_SIZE_2 || s0i.y > H + SHIP_SIZE_2);

	// We want to shoot when the other ship is in front of us and in close range.
	// If it is far, we only shoot if the trajectories are aligned.
	// If the angle to the other ship is not good for shooting, we want to rotate.

	// Work the sidelines. If we are close to the side and with our back to the
	// center, we want to warp (wrap) - this is the fastest way to have the
	// cannon facing the action.
	var rotation = s1.rotation;
	if (s1.x < SIDELINE) {
		// Left
		if (rotation < -PI_1_2) {
			input.goLeft = true;
		} else if (rotation > PI_1_2) {
			input.goRight = true;
		}
	} else if (s1.x > W - SIDELINE) {
		// Right
		if (0 > rotation && rotation > -PI_1_2) {
			input.goRight = true;
		} else if (PI_1_2 > rotation && rotation > 0) {
			input.goLeft = true;
		}
	} else if (s1.y < SIDELINE) {
		// Top
		if (-PI_1_2 > rotation) {
			input.goRight = true;
		} else if (0 > rotation && rotation > -PI_1_2) {
			input.goLeft = true;
		}
	} else if (s1.y > H - SIDELINE) {
		// Bottom
		if (PI_1_2 > rotation && rotation > 0) {
			input.goRight = true;
		} else if (rotation > PI_1_2) {
			input.goLeft = true;
		}
	}
	if (input.goRight || input.goLeft) {
		// If we make a move towards the sideline, get it done quickly
		input.goFaster = true;
		return input;
	}

	// Get the location info for both ships
	var cos = Math.cos(rotation),
		sin = Math.sin(rotation);
	var s1t = {
		x: s1.x + SHIP_SIZE_2 * cos,
		y: s1.y + SHIP_SIZE_2 * sin
	};
	var distance = game.physics.arcade.distanceBetween(s1t, s0i);
	var angle = game.physics.arcade.angleBetween(s1, s0i);
	var dAngle = Phaser.Math.wrapAngle(rotation - angle, true);
	var absdAngle = Math.abs(dAngle);

	// Shoot
	var angularSize = Math.atan(SHIP_SIZE_2 / distance);
	if (!noShoot &&
		((distance > 2 * SHIP_SIZE && Phaser.Math.within(dAngle, 0, angularSize)) ||
			(Phaser.Math.within(dAngle, 0, 0.5 * angularSize)))) {
		// If we are not too far and the angle is narrow enough, shoot
		input.doShoot = true;
	}

	// Steer, try to get the other ship in-front
	if (0 < dAngle) {
		// Left
		input.goLeft = true;
	} else {
		// Right
		input.goRight = true;
	}

	// Handle the speed
	if (input.doShoot) {
		// If we shoot, based on the distance, decide if we have to close the gap
		if (distance < SHOOT_RANGE_SHORT) {
			input.goSlower = true;
		} else if (distance > SHOOT_RANGE_LONG) {
			input.goFaster = true;
		}
	} else if (distance < SHOOT_RANGE_LONG && Phaser.Math.within(absdAngle, Math.PI, angularSize)) {
		// Other ship behind us and close, have to run away fast
		input.goFaster = true;
	} else if (distance > SHOOT_RANGE_LONG) {
		// Other ship is out of range, close the gap quickly.
		// A side effect if doing it here is that when the ships are far
		// and we are steering, the ship would follow a wide arc shape -
		// instead of a tight turn. This is probably better (and sure is
		// when tested head to head) as the ship is not a sitting duck.
		input.goFaster = true;
	} else if ((distance > SHOOT_RANGE_SHORT) && (distance < SHOOT_RANGE_LONG) &&
		(Phaser.Math.within(dAngle, Math.PI / 2, angularSize) ||
			Phaser.Math.within(dAngle, -Math.PI / 2, angularSize))) {
		// Other ship close on our side, run away fast
		input.goFaster = true;
	} else {
		if (absdAngle > Math.PI / 4) {
			// If we have a lot of angular distance to cover, slow down
			// for a tighter turn, unless we are very close, in which
			// case we want to break contact
			if (distance < SHIP_SIZE) {
				input.goFaster = true;
			} else {
				input.goSlower = true;
			}
		}
	}

	// Do some adjustment based on the level
	input = this._adjustInput(input);

	input.gameTime = GoZal.gameTime;
	return input;
};

// Inheritance Phaser style
// In a play mode that includes a server, all ships derive from this class.
// It can render the ship properly using prediction while respecting the
// authoritative state from the server using interpolation.
var ServerShip = function(name, options) {
	Ship.call(this, name, options);
};

ServerShip.prototype = Object.create(Ship.prototype);
ServerShip.prototype.constructor = ServerShip;

ServerShip.prototype._onCreate = function() {
	// We use the last input from the server. When the other player sends
	// new input (or no input), we will update.
	this._lastInput = {};
};

ServerShip.prototype._getInput = function() {
	// Do not take client input, use the last we've got from the server
	return this._lastInput;
};

ServerShip.prototype.setInput = function(input) {
	// We have new input from the server.
	this._lastInput = input;
};

ServerShip.prototype.onBeingHit = function(ship, bullet) {
	// Hit testing is handled exclusively by the server
	// Assume that it would agree with the client, so to ensure the
	// display is right, hide the bullet
	bullet.visible = false;
};

ServerShip.prototype.setState = function(state) {
	// Got the update from the server. Based on its game time value, we
	// can interpolate to the actual location. If our prediction was good,
	// this will result in a small update if at all.
	var dt = GoZal.gameTime - state.gameTime;
	Common.interpolatePosition(state, dt);
	this.x = state.x;
	this.y = state.y;

	// Handle being hit here:
	// The new state will come from the server. The client just has to update
	// the UI.
	if (state.hit) {
		this._setHitPoints(state.hitPoints);
		this._onHit();
	}

	this._setState(state);
};

ServerShip.prototype.doKill = function() {
	this._onDie();
};

// Inheritance Phaser style
// The PlayerShip is the one used by the player when in a network game. It is
// the same as a ServerShip, except that it takes input from the player and
// sends it to the server.
var PlayerShip = function(name, options) {
	this._lastInputData = 0;
	ServerShip.call(this, name, options);
};

PlayerShip.prototype = Object.create(ServerShip.prototype);
PlayerShip.prototype.constructor = PlayerShip;

// Use the same input controls as the single player mode
PlayerShip.prototype._onCreate = Ship.prototype._onCreate;

PlayerShip.prototype._getInput = Ship.prototype._getInput;

PlayerShip.prototype._updateServer = function(input) {
	GoZal.socket.onPlayerInput(input);
};

PlayerShip.prototype._filterInput = function(input) {
	// Let the base handle the filtering
	input = Ship.prototype._filterInput.call(this, input);

	// Tell the server what the user *wants* to do. There is no need to pass
	// the time of the action, this cannot be trusted and will be determined
	// by the server.

	// We only notify the server when there is a change.
	if (Common.hasInput(input)) {
		var packedInput = Common.packInput(input);
		if (packedInput !== this._lastInputData) {
			this._lastInputData = packedInput;
			// Sanitize the input to save bandwidth
			if (!input.goFaster) delete input.goFaster;
			if (!input.goSlower) delete input.goSlower;
			if (!input.goRight) delete input.goRight;
			if (!input.goLeft) delete input.goLeft;
			if (!input.doShoot) delete input.doShoot;
			this._updateServer(input);
		}
	} else {
		if (0 !== this._lastInputData) {
			this._lastInputData = 0;
			this._updateServer({
				gameTime: GoZal.gameTime
			});
		}
	}

	return input;
};

PlayerShip.prototype.setState = function(state) {
	// In case there is shooting, the player's ship already did that, so lower
	// the flag on this one. Shooting will never be initiated from the server
	// on behalf of the player :-)
	if (state.shoot) {
		var bullets = this._bullets.filter(function(bullet/*, index, bullets*/) {
			return !bullet.id;
		}, true);
		if (bullets.total > 0) {
			// console.log('setting bullet to id ' + state.shoot.bullet.id);
			bullets.first.id = state.shoot.bullet.id;
		} else {
			// This *can* happen. If the shot took place very close to the
			// edge, the bullet might have already been killed by the client,
			// before the server message came. But since this will not be a
			// hit, and the bullet wasn't actually a dud, this is fine.
			// console.log('cannot find bullet ' + state.shoot.bullet.id);
		}
		state.shoot = null;
	}
	state.nextFire = Math.max(state.nextFire, this._nextFire);

	// Let the base finish it
	ServerShip.prototype.setState.call(this, state);
};

// Inheritance Phaser style
// The PlayerShip2 is same as the PlayerShip, just with different controls.
var PlayerShip2 = function(name, options) {
	PlayerShip.call(this, name, options);
};

PlayerShip2.prototype = Object.create(PlayerShip.prototype);
PlayerShip2.prototype.constructor = PlayerShip2;

PlayerShip2.prototype._onCreate = function() {
	// Use different controls than the default so the other player
	// can control her ship :-)
	this._fastKey = game.input.keyboard.addKey(Phaser.Keyboard.W);
	this._slowKey = game.input.keyboard.addKey(Phaser.Keyboard.S);
	this._leftKey = game.input.keyboard.addKey(Phaser.Keyboard.A);
	this._rightKey = game.input.keyboard.addKey(Phaser.Keyboard.D);
	this._fireKey = game.input.keyboard.addKey(Phaser.Keyboard.X);
};

// Inheritance Phaser style
var DebugShip = function(name, options, ship0) {
	this._ship0 = ship0;

	// Use the text element to provide feedback
	this._$text = $('#text');

	Ship.call(this, name, options);
};

// Inheritance Phaser style
// The ServerMockShip is same as the ServerShip, with some automatic movement.
var ServerMockShip = function(name, options) {
	ServerShip.call(this, name, options);
};

ServerMockShip.prototype = Object.create(ServerShip.prototype);
ServerMockShip.prototype.constructor = ServerShip;

ServerMockShip.prototype._onCreate = function() {
	// Set up the trail paramateres (this is based on the BotFollow ship)
	this._trail = {
		turn: 60,
		speed: 5
	};
	this._trail.direction = 1;
	this._trail.acceleration = true;
	this._trail.tick = 0;
};

ServerMockShip.prototype._getInput = function() {
	var input = {};
	var self = this;

	function doSnakeLine() {
		var t = self._trail;
		++t.tick;

		// Start with the speed of the ship (only if needed)
		if (t.acceleration) {
			// 19 - 49: go fast
			// 69 - 99: go slow
			// else: no change
			var a = t.tick % 100;
			if (19 <= a && a <= 49) {
				input.goFaster = true;
			} else if (69 <= a && a <= 99) {
				input.goSlower = true;
			}
		}

		// Change direction based on the speed of the trail
		if (t.tick % t.speed > 1) return;
		var r = self.body.rotation;
		if (t.direction === 1) {
			if (r < t.turn) {
				input.goRight = true;
			} else {
				input.goLeft = true;
				t.direction = -1;
			}
		} else {
			if (r > -t.turn) {
				input.goLeft = true;
			} else {
				input.goRight = true;
				t.direction = 1;
			}
		}
	}

	doSnakeLine();

	return input;
};

ServerMockShip.prototype._updateServer = function(input) {
	GoZal.socket.onPlayerInputDebug(input);
};

ServerMockShip.prototype._filterInput = function(input) {
	// Do the same as the PlayerShip does
	PlayerShip.prototype._filterInput.call(this, input);

	return input;
};

DebugShip.prototype = Object.create(Ship.prototype);
DebugShip.prototype.constructor = DebugShip;

DebugShip.prototype._onCreate = function() {
	Ship.prototype._onCreate.call(this);

	this.x = 0.25 * W;
	this.y = H_2;
	this.body.velocity = {
		x: 0,
		y: 0
	};

	this.inputEnabled = true;
	this.input.enableDrag(true);
};

DebugShip.prototype._setState = function(state) {
	// Do not move
	state.velocity = {
		x: 0,
		y: 0
	};
	state.isAfterBurner = false;

	Ship.prototype._setState.call(this, state);
};

DebugShip.prototype.onBeingHit = function(ship, bullet) {
	// Ship is hit by a bullet, but it is immune.
	// Show the hit, but nothing else
	ship._onHit();
	bullet.kill();
};

DebugShip.prototype._onUpdate = function() {
	var distance = game.physics.arcade.distanceBetween(this, this._ship0).toFixed(2);
	var rotation = this.rotation.toFixed(2);
	var rotation1 = this._ship0.rotation.toFixed(2);
	var dRotation = Phaser.Math.wrapAngle(this.rotation - this._ship0.rotation, true).toFixed(2);
	var angle = game.physics.arcade.angleBetween(this, this._ship0).toFixed(2);
	var dAngle = Phaser.Math.wrapAngle(this.rotation - angle, true).toFixed(2);
	var position = '';
	if (Phaser.Math.within(dAngle, 0, 0.1)) {
		position += 'in front (narrow)';
	} else if (Phaser.Math.within(dAngle, Math.PI, 0.1)) {
		position += 'behind (narrow)';
	} else if (Phaser.Math.within(dAngle, 0, 0.3)) {
		position += 'behind (wide)';
	} else if (Phaser.Math.within(dAngle, Math.PI, 0.3)) {
		position += 'behind (wide)';
	} else if (dAngle > 0) {
		position += 'to the left';
	} else if (dAngle < 0) {
		position += 'to the right';
	}
	if (distance < SHOOT_RANGE_SHORT) {
		position += ', short range';
	} else if (distance < SHOOT_RANGE_LONG) {
		position += ', long range';
	} else {
		position += ', too far';
	}
	var decision = '';
	if (distance < SHOOT_RANGE_SHORT && Phaser.Math.within(dAngle, 0, 0.1)) {
		decision += 'Shoot, Go slow';
	} else if (distance < SHOOT_RANGE_LONG && Phaser.Math.within(dAngle, 0, 0.3)) {
		if (Phaser.Math.within(dRotation, 0, 0.1)) {
			decision += 'Shoot, Go fast';
		} else if (Phaser.Math.within(dRotation, -Math.PI, 0.1)) {
			decision += 'Shoot, Go fast';
		} else if (Phaser.Math.within(dRotation, Math.PI, 0.1)) {
			decision += 'Shoot, Go fast';
		} else {
			decision += 'No change';
		}
	} else {
		decision += 'No change';
	}

	// TODO:
	// Shoot decision has to be made based on the angular size of the target.
	// If it is within the cone, then based on the distance, make the shoot
	// decision, basically, can the ship move out of the cone in the time it
	// has to respond following the shot - calculated based on the speed of
	// the bullet.

	this._$text.html('<div class="debug-feedback"><ul class="text">' +
		'<li>Distance: ' + distance + '</li>' +
		'<li>Rotation (purple): ' + rotation + '</li>' +
		'<li>Rotation (orange): ' + rotation1 + '</li>' +
		'<li>Delta rotation: ' + dRotation + '</li>' +
		'<li>Angle: ' + angle + '</li>' +
		'<li>Normalized angle: ' + dAngle + '</li>' +
		'<li>Position: ' + position + '</li>' +
		'<li>Decision: ' + decision + '</li>' +
		'</ul></div>');
};

// The DebugShip2 is same as the DebugShip, just with different controls.
var DebugShip2 = function(name, options, ship0) {
	DebugShip.call(this, name, options, ship0);
};

DebugShip2.prototype = Object.create(DebugShip.prototype);
DebugShip2.prototype.constructor = DebugShip2;

DebugShip2.prototype._onCreate = function() {
	DebugShip.prototype._onCreate.call(this);

	this.x = 0.75 * W;

	// Use different controls than the default so the other player
	// can control her ship :-)
	this._fastKey = game.input.keyboard.addKey(Phaser.Keyboard.W);
	this._slowKey = game.input.keyboard.addKey(Phaser.Keyboard.S);
	this._leftKey = game.input.keyboard.addKey(Phaser.Keyboard.A);
	this._rightKey = game.input.keyboard.addKey(Phaser.Keyboard.D);
	this._fireKey = game.input.keyboard.addKey(Phaser.Keyboard.X);
};

DebugShip2.prototype._onUpdate = function() {};