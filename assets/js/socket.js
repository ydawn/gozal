/* global Common, Modes, GoZal, game, io, Socket, PlayerRoles */
var MDC = Common.MESSAGE_DATA.CLIENT;
var MDS = Common.MESSAGE_DATA.SERVER;

var Socket = function(callback) {
	var socket = io.connect();
	var self = this;

	socket.on('connected', function(data) {
		// TODO: WHAT HAPPENS WHEN A CONNECTION IS DROPPED?
		// IS THIS METHOD CALLED AGAIN???
		// TODO: handle disconnect.
		console.log('Socket connected with user id ' + data.userId);
		this.userId = data.userId;

		// Only register once to receive events
		socket.on(MDS.GAME_CREATED, function(data) {
			console.log('Game created: ' + data.gameId);
			// The next state will show the gameId to the host, so she can
			// share it with her rival. When done, she can click start, at
			// which point the game will wait till the opponent joins.

			// There is no explicit GAME_JOINED event for the host; joining
			// is implicite.
			this.role = PlayerRoles.GAME_HOST;
			GoZal.gameId = data.gameId;
			self._setState('host', data);

			if (Modes.DEBUG === GoZal.mode) {
				// DEBUG - join the other player now (so no need for another
				// client).
				GoZal.socket.joinGameDebug(data.gameId);
			}
		});

		socket.on(MDS.GAME_JOINED, function(data) {
			console.log('Game joined: ' + data.gameId);
			// This message is sent when the client joins the game. At this
			// point both players are in the room, so this message will be sent
			// to both. We can tell the mode based on the userId of the one that
			// joined (that would be the client, the host was automatically
			// placed in the room when the game was created).
			GoZal.gameId = data.gameId;
			this.role = PlayerRoles.GAME_CLIENT;
		});

		socket.on(MDS.GAME_READY, function(data) {
			console.log('Game ready: ' + data.gameId);
			// We will initialize the screen so the players can get oriented.
			// Highlight who you are, then start the countdown waiting for the
			// game to start. Hopefully, the message from the server will get to
			// the clients when the countdown is over :-)
			// This gives both clients enough time to create all objects and
			// draw the screen for the first time.
			self._setState('play', {
				state: data.state,
				countdown: data.countdown
			});
		});

		socket.on(MDS.GAME_STARTED, function(data) {
			console.log('Game start: ' + data.gameId);
			// This message is sent when the game initialization is done - the
			// server completed the initialization of all objects and both
			// clients now have to initialize to the same data.
			var currState = game.state.getCurrentState();
			if (currState.key === 'play') {
				currState.start();
			}
		});

		socket.on(MDS.GAME_UPDATED, function(data) {
			data = Common.unpackServerData(data);
			var currState = game.state.getCurrentState();
			if (currState.key === 'play' && data.gameId === GoZal.gameId) {
				currState.onServerUpdate(data);
			}
		});

		socket.on(MDS.GAME_OVER, function(data) {
			var currState = game.state.getCurrentState();
			if (currState.key === 'play' && data.gameId === GoZal.gameId) {
				currState.onServerGameOver(data.winner === this.userId);
			}
		});

		socket.on(MDS.GAME_CANCELED, function(data) {
			console.log('Game canceled: ' + data.gameId);
			self._setState('menu');
		});

		socket.on(MDS.GAME_ERROR, function(data) {
			self._setState('error', data);
		});

		socket.on('error', function(data) {
			self._setState('error', data);
		});

		callback();
	});

	this._socket = socket;
};

Socket.prototype._setState = function(state, arg) {
	game.state.start(state, false, false, arg);
};

Socket.prototype._verifyConnected = function() {
	if (!this._socket) {
		this._setState('error', 'Cannot create connection to the server.');
		return false;
	}
	if (!this._socket.connected) {
		this._setState('error', 'Cannot connect to the server.');
		return false;
	}
	return true;
};

Socket.prototype._log = function(message) {
	console.log('Client: ' + this._socket.userId + '[' + game.time.now + ']' + message);
};

Socket.prototype.createGame = function() {
	if (!this._verifyConnected()) return;

	this._log('requestCreateGame');
	this._socket.emit(MDC.CREATE_GAME, {
		userId: this._socket.userId
	});
};

Socket.prototype.joinGame = function(gameId) {
	if (!this._verifyConnected()) return;

	this._log('requestJoinGame for game ' + gameId);
	this._socket.emit(MDC.JOIN_GAME, {
		userId: this._socket.userId,
		gameId: gameId
	});
};

Socket.prototype.joinGameDebug = function(gameId) {
	if (!this._verifyConnected()) return;

	this._log('requestJoinGameDebug for game ' + gameId);
	this._socket.emit(MDC.JOIN_GAME, {
		userId: this._socket.userId + 'd',
		gameId: gameId
	});
};

Socket.prototype.cancelGame = function(gameId) {
	if (!this._verifyConnected()) return;

	this._log('requestCancelGame for game ' + gameId);
	this._socket.emit(MDC.CANCEL_GAME, {
		userId: this._socket.userId,
		gameId: gameId
	});
};

Socket.prototype.restartGame = function(gameId) {
	if (!this._verifyConnected()) return;

	this._log('requestRestartGame for game ' + gameId);
	this._socket.emit(MDC.RESTART_GAME, {
		userId: this._socket.userId,
		gameId: gameId
	});
};

Socket.prototype.viewGame = function(gameId) {
	if (!this._verifyConnected()) return;

	this._log('requestViewGame for game ' + gameId);
	this._socket.emit(MDC.VIEW_GAME, {
		userId: this._socket.userId,
		gameId: gameId
	});
};

Socket.prototype.onPlayerInput = function(input) {
	if (!this._verifyConnected()) return;
	if ('play' !== game.state.getCurrentState().key) return;

	// this._log('onPlayerInput: ' + JSON.stringify(input));
	this._socket.emit(MDC.ON_INPUT, Common.packClientData({
		userId: this._socket.userId,
		input: input,
		time: Common.getCurrentTimeDiff(GoZal.gameTime)
	}));
};

Socket.prototype.onPlayerInputDebug = function(input) {
	if (!this._verifyConnected()) return;
	if ('play' !== game.state.getCurrentState().key) return;

	// this._log('onPlayerInput: ' + JSON.stringify(input));
	this._socket.emit(MDC.ON_INPUT, Common.packClientData({
		userId: this._socket.userId + 'd',
		input: input
	}));
};