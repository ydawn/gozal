/* global Handlebars, Common, game */
/* exported debug, GAME_NAME, W_2, H_2, H_3, PI_1_2, REGENERATE_FREQUENCY, BULLET_SIZE,
FOLLOW_DIST2_GOOD, FOLLOW_DIST2_TOO_CLOSE, FOLLOW_MIN_TIME, SHIP_0_COLOR, SHIP_1_COLOR,
SHOOT_RANGE_SHORT, SHOOT_RANGE_LONG, SIDELINE, LOG, HEADER_TOP, HEADER_HEIGHT, MENU_LEFT,
MENU_HEIGHT, GENERIC_ERROR_MESSAGE, template,
Modes, PlayerRoles, PlayLevels, PracticeModes, DebugModes */
var debug = true;

var GAME_NAME = 'GoZal';

var W = Common.W,
	W_2 = W / 2;
var H = Common.H,
	H_2 = H / 2,
	H_3 = H / 3;

var PI_1_2 = 0.5 * Math.PI;

var Modes = {
	SINGLE_PLAYER: 1,
	MULTI_PLAYER: 2,
	PRACTICE: 3,
	DEBUG: 4
};

var PlayerRoles = {
	GAME_HOST: 1,
	GAME_CLIENT: 2,
	GAME_VIEWER: 3
};

var PlayLevels = {
	BEGINNER: 1,
	INTERMEDIATE: 2,
	EXPERT: 3
};

var PracticeModes = {
	SHOOT: 1,
	FOLLOW: 2,
	DODGE: 3
};

var DebugModes = {
	BOT_VS_BOT: 1,
	HUMAN_VS_HUMAN: 2,
	BOT_POSITION: 3,
	SERVER_MOCK_GAME: 4,
	SERVER_SHOOTING_RANGE: 5
};

var REGENERATE_FREQUENCY = Common.REGENERATE_FREQUENCY; // How much time between regenerates

// For hit area
var SHIP_SIZE = 50;
var SHIP_SIZE_2 = 0.5 * SHIP_SIZE;
var BULLET_SIZE = 16;

// Follow this ship params
var FOLLOW_DIST2_GOOD = SHIP_SIZE_2 * SHIP_SIZE_2; // Up to 30 pixels, center to center
var FOLLOW_DIST2_TOO_CLOSE = SHIP_SIZE * SHIP_SIZE;
var FOLLOW_MIN_TIME = 5000;

var SHIP_0_COLOR = 'purple';
var SHIP_1_COLOR = 'orange';

// Shooting
var SHOOT_RANGE_SHORT = 200;
var SHOOT_RANGE_LONG = 350;

// Sidelines
var SIDELINE = 50;

// Text elements
var HEADER_LEFT = 80;
var HEADER_TOP = 80;
var HEADER_HEIGHT = 80;
var MENU_LEFT = HEADER_LEFT;
var MENU_HEIGHT = 40;

// Errors
var GENERIC_ERROR_MESSAGE = 'An error occurred';

// Logging
function LOG(s) {
	var t = Date.now();
	console.log('T: ' + t + ': ' + s);
}

// Templating
var source = $('<div><h1>{{title}}</h1><p>{{{message}}}</p><ul class="menu">{{#menu entries}}<li><img src="/images/bullet.png"/><a href="#" data-state="{{state}}" data-mode="{{mode}}" data-key="{{key}}" data-arg="{{arg}}">{{menuString text key}}</a></li>{{/menu}}</ul></div>').html();
Handlebars.registerHelper('menu', function(entries, options) {
	var out = '';
	for (var i = 0; i < entries.length; ++i) {
		out += options.fn(entries[i]);
	}
	return out;
});

Handlebars.registerHelper('menuString', function(menu, key) {
	if (!key) key = menu.substr(0, 1);
	var posKey = menu.indexOf(key);
	var s;
	if (-1 === posKey) {
		s = '<span class="menu-accel">(' + key + ')</span> - ' + menu;
	} else {
		s = menu.substr(0, posKey) + '<span class="menu-accel">' + key + '</span>' + menu.substr(posKey + key.length);
	}
	return new Handlebars.SafeString(s);
});

var template = Handlebars.compile(source);

// Menu handler
$(function() {
	function handler(data) {
		var currState = game.state.getCurrentState();
		currState.next.call(currState, data.state, data.mode, data.arg);
	}

	$(document).on('click', 'ul.menu a', null, function(e/*, data*/) {
		e.preventDefault();
		handler($(this).data());
	});

	$(document).on('keydown', null, null, function(e) {
		var entries = $('ul.menu a');
		if (entries) {
			for (var i = 0; i < entries.length; ++i) {
				var $entry = $(entries[i]);
				var data = $entry.data();
				data.key = data.key ? data.key : $entry.text().substr(0, 1);
				if (String.fromCharCode(e.which).toLowerCase() === data.key.toString().toLowerCase() ||
					(e.which === 27 && data.key.toLowerCase() === 'esc') ||
					(e.which === 13 && data.key.toLowerCase() === 'enter')) {
					handler(data);
					break;
				}
			}
		}
	});
});