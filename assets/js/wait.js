/* global GoZal, game, template */
GoZal.Wait = function(/*game*/) {};

GoZal.Wait.prototype = {

	create: function() {
		game.add.tileSprite(0, 0, game.width, game.height, 'starfield');

		var context = {
			title: 'Wait...',
			message: 'Wait till your opponent joins the game',
			entries: [{
				text: 'Cancel',
				key: 'Esc',
				arg: 'cancel'
			}]
		};
		var html = template(context);
		$('#text').html(html);
	},

	next: function(state, mode, arg) {
		if ('cancel' === arg) {
			// Note that at this point the game is already created, so we have
			// to call cancel, not just go back to the menu
			GoZal.socket.cancelGame(GoZal.gameId);
		} else {
			game.state.start(state, true, false);
		}
	}
};