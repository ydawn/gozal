/* global exports*/
(function(exports) {

	exports.ASPECT_RATIO = 9 / 16;
	var W = exports.W = 1136;
	var H = exports.H = 640;
	var SHIP_SIZE_2 = 25; // For now
	var HIT_DIST = SHIP_SIZE_2 - 5;
	var HIT_DIST_SQR = HIT_DIST * HIT_DIST;

	// The idea of the damper is to reduce the number of user inputs per cycle.
	// There is no need for 60 inputs per second, we just have to apply the
	// fewer inputs to all frames in the cycle.
	// var INPUT_FREQUENCY = exports.INPUT_FREQUENCY = 3; // How many frames between user inputs

	exports.CLIENT_FRAME_MS = 1000 / 60;
	var SERVER_FRAME_MS = exports.SERVER_FRAME_MS = 1000 / 20;
	exports.REGENERATE_FREQUENCY = 250; // How much time between updates
	exports.COUNT_DOWN = 3; // 3 seconds

	exports.MESSAGE_DATA = {
		CLIENT: {
			CREATE_GAME: 0,
			JOIN_GAME: 1,
			VIEW_GAME: 2,
			CANCEL_GAME: 3,
			RESTART_GAME: 4,
			ON_INPUT: 9
		},
		SERVER: {
			GAME_CREATED: 10,
			GAME_JOINED: 11,
			GAME_READY: 12,
			GAME_STARTED: 13,
			GAME_UPDATED: 14,
			GAME_OVER: 15,
			GAME_ERROR: 16,
			GAME_CANCELED: 17,
			GAME_RESTARTED: 18
		}
	};

	var ROCK_DATA = exports.ROCK_DATA = [{
		// BG
		count: 3,
		scale: {
			base: 0.15,
			adj: 0.15
		},
		velocity: {
			base: 0.009,
			adj: 0.005
		},
		altitude: {
			base: 0.05,
			adj: 0.8
		}
	}, {
		// FG
		count: 3,
		scale: {
			base: 0.40,
			adj: 0.25
		},
		velocity: {
			base: 0.015,
			adj: 0.009
		},
		altitude: {
			base: 0.20,
			adj: 0.6
		}
	}];

	var SHIP_DATA = exports.SHIP_DATA = {
		// Speed
		minSpeed: 50,
		cruiseSpeed: 100,
		maxSpeed: 200, // With the after-burner on
		acceleration: 2,
		deceleration: 1,

		// Angular speed
		//maxAngularSpeed: 200,
		//angularDrag: 200,
		// angularAcceleration: INPUT_FREQUENCY * Math.PI / 180,
		angularAcceleration: Math.PI / 180,

		// Energy
		maxEnergy: 1000,
		energyDischarge: 3, // Energy spent when on high speed
		energyRegenerate: 5, // per regenerate cycle

		// Weapon
		maxCannonCharge: 100,
		cannonRecharge: 330, // time to recharge between shots
		cannonDischarge: 5, // how much power lost each shot
		cannonRegenerate: 0.5,
		bulletLimit: 10,
		bulletSpeed: 300,

		// Health
		maxHitPoints: 1000,
		hitDamage: 150,
		hitRotation: Math.PI / 36,
		hitPointsRegenerate: 2,
		hitBadlyThreshold: 0.2
	};

	// Return 0 or 1
	var coinToss = exports.coinToss = function() {
		return Math.round(Math.random());
	};

	// Return a random number from a range (base + r * adjustment)
	var randomRange = exports.randomRange = function(range) {
		return range.base + range.adj * Math.random();
	};

	exports.RockState = function(type, x, y, velocity, scale) {
		var options = ROCK_DATA[type];
		this.scale = scale || randomRange(options.scale); // * (coinToss() ? 1 : -1);
		this.x = x || Math.random() * W;
		this.y = y || randomRange(options.altitude) * H;
		this.velocity = velocity || {
			x: randomRange(options.velocity),
			y: 0
		};
	};
	var RockState = exports.RockState;

	exports.ShipState = function(gameTime, x, y, velocity, rotation, hitPoints, charge, energy, nextFire, isOnFire, isAfterBurner, isBreakOn) {
		this.gameTime = gameTime;
		this.x = x;
		this.y = y;
		this.velocity = velocity || {
			x: SHIP_DATA.cruiseSpeed,
			y: 0
		};
		this.rotation = rotation || 0;
		this.hitPoints = hitPoints || SHIP_DATA.maxHitPoints;
		this.charge = (typeof(charge) !== 'undefined' && charge !== null) ? charge : SHIP_DATA.maxCannonCharge;
		this.energy = (typeof(energy) !== 'undefined' && energy !== null) ? energy : SHIP_DATA.maxEnergy;
		this.nextFire = nextFire || 0;
		this.isOnFire = isOnFire || false;
		this.isAfterBurner = isAfterBurner || false;
		this.isBreakOn = isBreakOn || false;
		this.shoot = null;
		this.hit = null;
	};
	var ShipState = exports.ShipState;

	ShipState.prototype.clone = function() {
		// Only clone the state attributes. Do not clone extra attributes
		// like shoot etc.
		return new ShipState(
			this.gameTime, this.x, this.y, {
				x: this.velocity.x,
				y: this.velocity.y
			}, this.rotation,
			this.hitPoints, this.charge, this.energy, this.nextFire,
			this.isOnFire, this.isAfterBurner, this.isBreakOn);
	};

	// Initialize the state of the game
	exports.initGameState = function() {
		// Rocks
		function RandomArray(start, end) {
			var array = [];
			for (var i = start; i <= end; ++i) {
				array.push(i);
			}
			for (i = array.length - 1; i > 0; --i) {
				var j = Math.floor(Math.random() * (i + 1));
				var temp = array[i];
				array[i] = array[j];
				array[j] = temp;
			}
			return array;
		}

		var rockImages = RandomArray(0, 9);
		var lastImageUsed = 0;

		function initRocks(type) {
			var rocks = [];
			var count = ROCK_DATA[type].count + coinToss();
			for (var i = 0; i < count; ++i, ++lastImageUsed) {
				var rockState = new RockState(type);
				rockState.imageIndex = rockImages[lastImageUsed];
				rocks.push(rockState);
			}
			return rocks;
		}

		// Game time
		var gameTime = 0;

		// Ships
		var x = (0 === coinToss()) ? 1 : -1; // Randomize x location
		var y = (0 === coinToss()) ? 1 : -1; // Randomize y location
		var d = (0 === coinToss()) ? 1 : -1; // Randomize direction
		return {
			rocksBG: initRocks(0),
			rocksFG: initRocks(1),
			ship0: new ShipState(
				gameTime, W * (0.5 + x * 0.25), H * (0.5 + y * 0.25), {
					x: -d * 100,
					y: 0
				}, Math.PI * (0.5 + d * 0.5)),
			ship1: new ShipState(
				gameTime, W * (0.5 - x * 0.25), H * (0.5 - y * 0.25), {
					x: +d * 100,
					y: 0
				}, Math.PI * (0.5 - d * 0.5))
		};
	};

	exports.hasInput = function(input) {
		// Return true when at least one change is requested
		return input.goFaster || input.goSlower || input.goRight || input.goLeft || input.doShoot || false;
	};

	exports.filterInput = function(input, state, isServer) {
		// When filtering on the server side, the input time will be
		// different due to latency. Therefore, we need to add some grace
		// time to the time based filter, thus avoiding a no-shoot call
		// by the server when the (honest) client already shot (resulting
		// in a dud bullet that cannot score a qualifying hit).
		var timeGrace = isServer ? SERVER_FRAME_MS : 0;
		var chargeGrace = isServer ? SHIP_DATA.cannonDischarge : 0;

		// Apply some basic rules
		if (input.doShoot) {
			if (state.charge + chargeGrace < SHIP_DATA.cannonDischarge) {
				// Cannon is out
				input.doShoot = false;
			} else if (input.gameTime + timeGrace < state.nextFire) {
				// Cannon is not ready to go yet
				input.doShoot = false;
			} else {
				// Get the tip of the ship
				var cos = Math.cos(state.rotation),
					sin = Math.sin(state.rotation);
				var x = state.x + SHIP_SIZE_2 * cos,
					y = state.y + SHIP_SIZE_2 * sin;
				if (x < 0 || x > W || y < 0 || y > H) {
					// Cannot shoot, out of the play area
					input.doShoot = false;
				}
			}
		}
		if (input.goFaster) {
			if (state.isOnFire || state.energy <= 0) {
				// Cannot turn on after burner
				input.goFaster = false;
			}
		}

		return input;
	};

	var interpolatePosition = exports.interpolatePosition = function(state, deltaMS) {
		if (0 === deltaMS) {
			// Nothing to interpolate
			return;
		}
		// In a perfect world, the math works like so:
		// x1 = x0 + v.x * dt
		// x0, x1: pixel
		// v.x: pixel / sec^2
		// dt: sec^2 -> with an update each frame, this is 1/60 = 0.016
		// We want to convert the time differential (in ms) to number of frames,
		// then multiply by the factor 0.016.
		function DT(deltaMS) {
			//return FPS * deltaMS / CLIENT_FRAME_MS;
			return deltaMS / 1000;
		}

		var dt = DT(deltaMS);
		var v = state.velocity;
		state.x += v.x * dt;
		state.y += v.y * dt;
	};

	// Using an existing state, apply the subsequent input. The idea is that
	// motion continues based on the state until the input - that has a
	// timestamp - is applied.
	// The bullets fired *at* this ship are considered part of the input.
	// IMPORTANT: because we might need to update velocity on any cycle, there
	// needs to be a server side update on any cycle, even if the input is
	// empty (in which case, only velocity might change and hit-tests must be
	// done)
	exports.processInput = function(state, input, bullets) {

		// The job is done is two stages:
		// 1. Interpolate position based on last state.
		// 2. Apply the new input to get to the new state.

		var newState = state.clone();
		var deltaMS = input.gameTime - state.gameTime;
		var shipOld = {
			x: state.x,
			y: state.y
		};
		interpolatePosition(newState, deltaMS);

		// Handle speed change (speed is a vector)
		// Look into: game.physics.arcade.accelerationFromRotation(sprite.rotation, 200, sprite.body.acceleration);
		// Consider optimizing when no speed change, only shooting.
		var v = getVelocity(state.velocity);
		if (input.goFaster) {
			// Accelerate
			v += SHIP_DATA.acceleration;
			newState.isAfterBurner = true;
			newState.isBreakOn = false;
			newState.energy = clamp(newState.energy - SHIP_DATA.energyDischarge, 0, SHIP_DATA.maxEnergy);
		} else if (input.goSlower) {
			// Break
			v -= SHIP_DATA.acceleration;
			newState.isAfterBurner = false;
			newState.isBreakOn = true;
		} else {
			// Cruise
			newState.isAfterBurner = newState.isBreakOn = false;
			if (!isEqual(v, SHIP_DATA.cruiseSpeed, 1)) {
				// Apply drag (go faster if we are below the cruise speed)
				v -= (v > SHIP_DATA.cruiseSpeed) ? SHIP_DATA.deceleration : -SHIP_DATA.deceleration;
			}
		}

		// Make sure the ship moves at the speed range allowed
		v = clamp(v, SHIP_DATA.minSpeed, SHIP_DATA.maxSpeed);

		// Handle rotation
		if (input.goRight) {
			newState.rotation += SHIP_DATA.angularAcceleration * SHIP_DATA.cruiseSpeed / v;
		}
		if (input.goLeft) {
			newState.rotation -= SHIP_DATA.angularAcceleration * SHIP_DATA.cruiseSpeed / v;
		}
		newState.rotation = wrapAngle(newState.rotation);

		setVelocityFromRotation(newState.velocity, v, newState.rotation);

		newState.x = wrap(newState.x, -SHIP_SIZE_2, W + SHIP_SIZE_2);
		newState.y = wrap(newState.y, -SHIP_SIZE_2, H + SHIP_SIZE_2);

		// Handle shooting. The assumption is that there can be at most one shot
		// fired in a single update cycle - the length of which depends on the
		// server update rate, but should be much better than the shooting rate.
		if (state.shoot) {
			// If there is already a shot fired in this cycle, carry it to the
			// final update of this cycle.
			newState.shoot = state.shoot;
		} else if (input.doShoot) {
			newState.shoot = {
				gameTime: input.gameTime,
				bullet: getShotData(newState.x, newState.y, newState.velocity, newState.rotation)
			};
			newState.nextFire = input.gameTime + SHIP_DATA.cannonRecharge;
			newState.charge = Math.max(newState.charge - SHIP_DATA.cannonDischarge, 0);
		} else {
			newState.shoot = null;
		}
		// if (input.doShoot) {
		// 	newState.shoot = {
		// 		gameTime: input.gameTime,
		// 		bullet: getShotData(newState.x, newState.y, newState.velocity, newState.rotation)
		// 	};
		// 	newState.nextFire = input.gameTime + SHIP_DATA.cannonRecharge;
		// 	newState.charge -= SHIP_DATA.cannonDischarge;
		// } else if (state.shoot) {
		// 	// If there is already a shot fired in this cycle, carry it to the
		// 	// final update of this cycle.
		// 	newState.shoot = state.shoot;
		// } else {
		// 	newState.shoot = null;
		// }

		// Handle getting hit. Same assumption as before - only one hit per
		// server update cycle.
		if (state.hit) {
			// If there is already a hit, carry it to the final update
			newState.hit = state.hit;
		}
		if (bullets && bullets.length) {
			var i = 0,
				bullet, bulletOld;
			while (i < bullets.length) {
				bullet = bullets[i];
				bulletOld = {
					x: bullet.x,
					y: bullet.y
				};
				interpolatePosition(bullet, deltaMS);
				if (isHit(shipOld, newState, bulletOld, bullet)) {
					bullets.splice(i, 1);
					newState = onHit(newState, bullet);
					break;
					// TODO: fix this. Bullets are not properly disposed of with this code.
				} else if (bullet.x < 0 || bullet.x > W || bullet.y < 0 || bullet.y > H) {
					bullets.splice(i, 1); // Out of bounds, remove it
				} else {
					++i;
				}
			}
		}

		// Set the "on fire" state correct for now
		newState.isOnFire = (DAMAGE_THRESHOLD > newState.hitPoints);

		// Set the game time
		newState.gameTime = input.gameTime;

		return newState;
	};

	var isHit = exports.isHit = function(shipOld, shipNew, bulletOld, bulletNew) {
		// Assume:
		// 	The ship is slow, large and round-shaped.
		// 	The bullet is fast, small and point-shaped.
		// (sx0, sy0) -> (sx1, sy1) - the ship coordinates
		// sr - the ship radius
		// (bx0, by0) -> (bx1, by1) - the bullet coordinates
		// For the purpose of hit-testing, assume the location of the
		// ship is: (0.5(sx0 + sx1), 0.5(sy0 + sy1)) = (sX, sY) = S
		// So the math is looking for any intersection of the segment
		// created by the path of the bullet and the area of the ship.
		var S = {
			x: avg(shipOld.x, shipNew.x),
			y: avg(shipOld.y, shipNew.y)
		};
		if (!isShipRectOverlappingBulletPath(S, bulletOld, bulletNew)) {
			return false; // Rect optimization
		}
		return HIT_DIST_SQR >= distToSegmentSquared(S, bulletOld, bulletNew);
	};

	var PI_1_3 = Math.PI / 3,
		PI_2_3 = 2 * PI_1_3;
	var DAMAGE_THRESHOLD = exports.DAMAGE_THRESHOLD = SHIP_DATA.hitBadlyThreshold * SHIP_DATA.maxHitPoints;

	var onHit = exports.onHit = function(state, bullet) {
		var newState = state;
		newState.hit = bullet;

		// Reduce hit points by the quality of the hit
		function hitQuality(angleShip, angleBullet) {
			var angleHit = Math.abs(wrapAngle(angleBullet - angleShip));
			if (angleHit < PI_1_3) return 1;
			else if (angleHit < PI_2_3) return 0.7;
			else return 0.4;
		}
		newState.hitPoints -= hitQuality(state.rotation, bullet.rotation) * SHIP_DATA.hitDamage;
		if (newState.hitPoints <= 0) {
			newState.die = true;
		}

		// Change direction following a hit
		function hitBump(angleShip, angleBullet) {
			return (angleShip > angleBullet) ? 1 : -1;
		}
		newState.rotation += hitBump(state.rotation, bullet.rotation) * SHIP_DATA.hitRotation;
		var v = newState.velocity;
		setVelocityFromRotation(v, getVelocity(v), newState.rotation);

		return newState;
	};

	exports.onRegenerate = function(state) {
		var newState = state;

		newState.hitPoints = Math.min(state.hitPoints + SHIP_DATA.hitPointsRegenerate, SHIP_DATA.maxHitPoints);
		if (DAMAGE_THRESHOLD <= newState.hitPoints) {
			// Bad damage fixed, yey
			newState.onFire = false;
		}
		newState.charge = Math.min(state.charge + SHIP_DATA.cannonRegenerate, SHIP_DATA.maxCannonCharge);
		newState.energy = Math.min(state.energy + SHIP_DATA.energyRegenerate, SHIP_DATA.maxEnergy);

		return newState;
	};

	var getShotData = function(x, y, velocity, rotation) {
		var cos = Math.cos(rotation),
			sin = Math.sin(rotation);
		return {
			x: x + SHIP_SIZE_2 * cos,
			y: y + SHIP_SIZE_2 * sin,
			velocity: {
				x: velocity.x + SHIP_DATA.bulletSpeed * cos,
				y: velocity.y + SHIP_DATA.bulletSpeed * sin
			},
			rotation: rotation
		};
	};

	// Generic helpers (similar to Phaser's, so we don't have to include it on
	// the server side).
	var isEqual = exports.isEqual = function(val0, val1, tolerance) {
		return Math.abs(val0 - val1) < tolerance;
	};

	var wrap = exports.wrap = function(x, min, max) {
		var range = max - min;
		var result = (x - min) % range;
		if (result < 0) {
			result += range;
		}
		return result + min;
	};

	var wrapAngle = exports.wrapAngle = function(x) {
		return wrap(x, -Math.PI, Math.PI);
	};

	var clamp = exports.clamp = function(x, min, max) {
		if (x < min) return min;
		else if (x > max) return max;
		else return x;
	};

	// exports.getDistance = function(x0, y0, x1, y1) {
	// 	var dx = x0 - x1, dy = y0 - y1;
	// 	return Math.sqrt(dx * dx + dy * dy);
	// };

	// The is used to optimize hit testing. For bullets that are far
	// from the ship, it should return following the first test, so
	// it should be pretty efficient.
	var isShipRectOverlappingBulletPath = function(ship, bullet0, bullet1) {
		if (ship.x + SHIP_SIZE_2 <= Math.min(bullet0.x, bullet1.x)) {
			return false;
		}
		if (ship.y + SHIP_SIZE_2 <= Math.min(bullet0.y, bullet1.y)) {
			return false;
		}

		if (Math.max(bullet0.x, bullet1.x) <= ship.x - SHIP_SIZE_2) {
			return false;
		}
		if (Math.max(bullet0.y, bullet1.y) <= ship.y - SHIP_SIZE_2) {
			return false;
		}

		return true;
	};

	var avg = function(x, y) {
		return 0.5 * (x + y);
	};

	var sqr = function(x) {
		return x * x;
	};

	var dist2 = exports.getDistance2 = function(v, w) {
		return sqr(v.x - w.x) + sqr(v.y - w.y);
	};

	// exports.getDistance = function (v, w) {
	// 	return Math.sqrt(dist2(v, w));
	// };

	var distToSegmentSquared = function(p, v, w) {
		var l2 = dist2(v, w);
		if (l2 === 0) return dist2(p, v);
		var t = ((p.x - v.x) * (w.x - v.x) + (p.y - v.y) * (w.y - v.y)) / l2;
		if (t < 0) return dist2(p, v);
		if (t > 1) return dist2(p, w);
		return dist2(p, {
			x: v.x + t * (w.x - v.x),
			y: v.y + t * (w.y - v.y)
		});
	};

	exports.getVelocity2 = function(v) {
		return sqr(v.x) + sqr(v.y);
	};

	var getVelocity = exports.getVelocity = function(v) {
		return Math.sqrt(sqr(v.x) + sqr(v.y));
	};

	var setVelocityFromRotation = exports.setVelocityFromRotation = function(v, V, r) {
		var cos = Math.cos(r),
			sin = Math.sin(r);
		v.x = V * cos;
		v.y = V * sin;
	};

	// Socket.io helpers
	var GO_FASTER = 1;
	var GO_SLOWER = 2;
	var GO_RIGHT = 4;
	var GO_LEFT = 8;
	var DO_SHOOT = 16;

	// var getCurrentTimeDiff = exports.getCurrentTimeDiff = function(t) {
	// 	return (t - Date.now());
	// };

	var packInput = exports.packInput = function(input) {
		var p = 0;
		if (input.goFaster) p |= GO_FASTER;
		if (input.goSlower) p |= GO_SLOWER;
		if (input.goRight) p |= GO_RIGHT;
		if (input.goLeft) p |= GO_LEFT;
		if (input.doShoot) p |= DO_SHOOT;
		return p;
	};

	var packGameTime = exports.packGameTime = function(t) {
		// Bitwise OR with zero will convert a floating point number to integer
		return (t * 1000) | 0;
	};

	exports.packClientData = function(data) {
		return [
			data.userId,
			packInput(data.input),
			packGameTime(data.input.gameTime),
			data.time
		];
	};

	var unpackInput = exports.unpackInput = function(inputData) {
		function has(m) {
			return m === (inputData & m);
		}

		return {
			goFaster: has(GO_FASTER),
			goSlower: has(GO_SLOWER),
			goRight: has(GO_RIGHT),
			goLeft: has(GO_LEFT),
			doShoot: has(DO_SHOOT)
		};
	};

	var unpackGameTime = exports.unpackGameTime = function(t) {
		return t / 1000.0;
	};

	exports.unpackClientData = function(data) {
		// function has(m) {
		// 	return m === (data[1] & m);
		// }

		var input = unpackInput(data[1]);
		input.gameTime = unpackGameTime(data[2]);

		return {
			userId: data[0],
			input: input,
			time: data[3]
		};
	};

	var ON_FIRE = 1;
	var AFTER_BURNER = 2;
	var BREAK_ON = 4;

	exports.packServerData = function(data) {
		// PVR stands for Position, Velocity, Rotation (which can be had from V)
		function packPVR(o) {
			// Use the round method to turn the floating point numbers to integrals.
			// This saves a bit on the band width (sometimes, 25% overall).
			return [Math.round(o.x), Math.round(o.y), Math.round(o.velocity.x), Math.round(o.velocity.y), o.rotation];
		}

		function packModifiers(shipState) {
			var p = 0;
			if (shipState.isOnFire) p |= ON_FIRE;
			if (shipState.isAfterBurner) p |= AFTER_BURNER;
			if (shipState.isBreakOn) p |= BREAK_ON;
			return p;
		}

		function packShoot(shootData) {
			if (null === shootData) return null;
			return [packGameTime(shootData.gameTime), packPVR(shootData.bullet), shootData.bullet.id];
		}

		function packHit(hitData) {
			if (null === hitData) return null;
			return [packPVR(hitData), hitData.id];
		}

		function packShipState(shipState) {
			return [
				packGameTime(shipState.gameTime),
				packPVR(shipState),
				shipState.hitPoints, shipState.charge, shipState.energy, shipState.nextFire,
				packModifiers(shipState),
				packShoot(shipState.shoot),
				packHit(shipState.hit)
			];
		}

		return [
			data.gameId,
			packGameTime(data.gameTime),
			packShipState(data.state.ship0),
			packShipState(data.state.ship1),
			packInput(data.input.ship0),
			packInput(data.input.ship1)
		];
	};

	exports.unpackServerData = function(data) {
		function has(d, m) {
			return m === (d & m);
		}

		function unpackShoot(shootData) {
			if (null === shootData) return null;
			var bulletPVR = shootData[1];
			return {
				gameTime: unpackGameTime(shootData[0]),
				bullet: {
					x: bulletPVR[0],
					y: bulletPVR[1],
					velocity: {
						x: bulletPVR[2],
						y: bulletPVR[3]
					},
					rotation: bulletPVR[4],
					id: shootData[2]
				}
			};
		}

		function unpackHit(hitData) {
			if (null === hitData) return null;
			var hitPVR = hitData[0];
			return {
				x: hitPVR[0],
				y: hitPVR[1],
				velocity: {
					x: hitPVR[2],
					y: hitPVR[3]
				},
				rotation: hitPVR[4],
				id: hitData[1]
			};
		}

		function unpackShipState(shipStateData) {
			var shipPVR = shipStateData[1];
			var modifiers = shipStateData[6];
			return {
				gameTime: unpackGameTime(shipStateData[0]),
				x: shipPVR[0],
				y: shipPVR[1],
				velocity: {
					x: shipPVR[2],
					y: shipPVR[3]
				},
				rotation: shipPVR[4],
				hitPoints: shipStateData[2],
				charge: shipStateData[3],
				energy: shipStateData[4],
				nextFire: shipStateData[5],
				isOnFire: has(modifiers, ON_FIRE),
				isAfterBurner: has(modifiers, AFTER_BURNER),
				isBreakOn: has(modifiers, BREAK_ON),
				shoot: unpackShoot(shipStateData[7]),
				hit: unpackHit(shipStateData[8])
			};
		}

		return {
			gameId: data[0],
			gameTime: unpackGameTime(data[1]),
			state: {
				ship0: unpackShipState(data[2]),
				ship1: unpackShipState(data[3])
			},
			input: {
				ship0: unpackInput(data[4]),
				ship1: unpackInput(data[5])
			}
		};
	};

})(typeof exports === 'undefined' ? this.Common = {} : exports);