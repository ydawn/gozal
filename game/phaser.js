var gameloop = require('node-gameloop');
var sprintf = require('sprintf-js').sprintf;
var assert = require('chai').assert;
var logger = require('../utils/logger');
var Queue = require('../utils/queue');
var common = require('../game/common.js');

var DEBUG_MAX_TIME = 5000000;

module.exports = function(app) {

	function log() {
		console.log(sprintf.apply(this, arguments));
	}

	function logError() {
		logger.error(sprintf.apply(this, arguments));
	}

	log('Initializing Server side');

	var CLIENT_FRAME_MS = common.CLIENT_FRAME_MS;
	var SERVER_FRAME_MS = common.SERVER_FRAME_MS;
	var COUNT_DOWN = common.COUNT_DOWN;

	// Socket.io messages
	var MDC = common.MESSAGE_DATA.CLIENT;
	var MDS = common.MESSAGE_DATA.SERVER;

	var SD = common.SHIP_DATA;
	var io = app.io;

	var GameData = function(gameId) {
		this.gameId = gameId;
		this.gameLoopId = null;
		this.bulletId = 0;
		this.gameTime = 0;
		this.gameTimeTick = Date.now();
		this.inputQueue0 = new Queue();
		this.inputQueue1 = new Queue();
		this.lastInput0 = {};
		this.lastInput1 = {};
		this.state = {
			ship0: null,
			ship1: null,
			bullets0: [],
			bullets1: [],
			tick: 0,
			regenerate: 0
		};
		this.player0 = null; // Player 0 is the host
		this.player1 = null;
		this.winner = null;
	};

	GameData.prototype.setState = function(state) {
		this.state.ship0 = state.ship0;
		this.state.ship1 = state.ship1;
	};

	GameData.prototype.addInput = function(userId, input) {
		if (userId === this.player0) {
			this.inputQueue0.enqueue(input);
		} else if (userId === this.player1) {
			// console.log('Input: ' + JSON.stringify(input));
			this.inputQueue1.enqueue(input);
		} else {
			assert(false, 'Input must match one of the players');
		}
	};

	var gameData = null;

	function playGame(gameData) {
		// Tell the clients to start
		var gameId = gameData.gameId;
		log('IO: DEBUG START GAME');
		log('***********************************************');
		io.to(gameId).emit(MDS.GAME_STARTED, {
			gameId: gameId
		});
		gameData.gameTimeTick = Date.now();

		// var gameData = new GameData(gameId);
		var lastState = gameData.state;

		function updateState(dt) {
			// We have to update for every client "tick". If there is an input
			// from the client, use it; otherwise, use the default input.
			var updates0 = [],
				updates1 = [];

			// We get the time of the first client tick by adding the tick length
			// to the time of the last processed client tick.
			var lastTick = lastState.tick;
			for (var tickTime = lastTick + CLIENT_FRAME_MS; tickTime <= gameData.gameTime + dt; tickTime += CLIENT_FRAME_MS) {
				updates0.push({
					gameTime: tickTime
				});
				updates1.push({
					gameTime: tickTime
				});
				lastTick = tickTime;
			}

			// Put any input we got from the client in the correct slot,
			// replacing the default updates with the inputs.
			var updateCount = updates0.length;
			assert(updateCount > 0, 'Must have updates to process');

			function mergeInputQueue(inputQueue, updates, lastInput) {
				for (var slot = 0; slot < updateCount; ++slot) {
					var update = updates[slot];
					var slotTime = update.gameTime;
					var input = inputQueue.peek();
					if (null !== input && input.gameTime < slotTime) {
						lastInput = inputQueue.dequeue();
					}
					updates[slot] = lastInput;
					updates[slot].gameTime = slotTime;
				}
				return lastInput;
				//assert(inputQueue.isEmpty(), 'Input queue must be fully processed');
			}

			lastState.hasToUpdateClients = (!gameData.inputQueue0.isEmpty() || !gameData.inputQueue1.isEmpty());
			gameData.lastInput0 = mergeInputQueue(gameData.inputQueue0, updates0, gameData.lastInput0);
			gameData.lastInput1 = mergeInputQueue(gameData.inputQueue1, updates1, gameData.lastInput1);
			lastState.hasToUpdateClients |= common.hasInput(gameData.lastInput0) || common.hasInput(gameData.lastInput1);

			// Process the inputs in order.
			// The lastState always holds the timing info for the last state.
			function regenerate(state) {
				if (state.hitPoints === SD.maxHitPoints &&
					state.energy === SD.maxEnergy &&
					state.charge === SD.maxCannonCharge) {
					return false;
				}
				state = common.onRegenerate(state);
				return true;
			}

			// Prepare the local variables and references
			var lastState0 = lastState.ship0,
				lastState1 = lastState.ship1;
			var bullets0 = lastState.bullets0,
				bullets1 = lastState.bullets1;
			var nextState0 = lastState0.shoot = lastState0.hit = null;
			var nextState1 = lastState1.shoot = lastState1.hit = null;

			// Process the updates for both ships in tandem
			for (var i = 0; i < updateCount; ++i) {
				// We do not trust the client - have to re-apply filtering
				var input0 = common.filterInput(updates0[i], lastState0, true);
				var input1 = common.filterInput(updates1[i], lastState1, true);

				// Get the next state
				nextState0 = common.processInput(lastState0, input0, bullets1);
				nextState1 = common.processInput(lastState1, input1, bullets0);

				// Handle regeneration and downstream routine updates
				if (updates0[i].gameTime > lastState.regenerate + common.REGENERATE_FREQUENCY) {
					// Regenerate
					lastState.hasToUpdateClients |= regenerate(nextState0);
					lastState.hasToUpdateClients |= regenerate(nextState1);
					lastState.regenerate = updates0[i].gameTime;

					// Even if regenerate did not do anything, we want to
					// update the client. This will ensure no skew of the
					// ship location when there is no update for a long(ish)
					// time.
					lastState.hasToUpdateClients = true;
				}

				// Move to the next sub-cycle
				lastState0 = nextState0;
				lastState1 = nextState1;
				if (lastState0.die || lastState1.die) break;
				lastState.hasToUpdateClients |= (null !== lastState0.hit);
				lastState.hasToUpdateClients |= (null !== lastState1.hit);
			}

			// Handle shooting and hitting (one per cycle); bullet id is
			// assigned server side, so that when there is a hit, the client
			// can be told to remove this bullet from the screen
			if (lastState0.shoot) {
				lastState0.shoot.bullet.id = ++gameData.bulletId;
				bullets0.push(lastState0.shoot.bullet);
				lastState.hasToUpdateClients = true;
			}
			if (lastState1.shoot) {
				lastState1.shoot.bullet.id = ++gameData.bulletId;
				bullets1.push(lastState1.shoot.bullet);
				lastState.hasToUpdateClients = true;
			}
			if (lastState1.die) {
				lastState.winner = gameData.player0;
			} else if (lastState0.die) {
				lastState.winner = gameData.player1;
			}

			// Keep the final state in the global variable
			lastState.ship0 = lastState0;
			lastState.ship1 = lastState1;

			lastState.tick = lastTick;
			// lastState.hasToUpdateClients = true;
			return lastState;
		}

		// Start the game loop at the given rate
		gameData.gameLoopId = gameloop.setGameLoop(function(delta) {
			// delta is the time from the last frame.
			// We found that this is inaccurate and leads to a time drift
			// on the server side, so instead we keep the last tick and
			// calculate based on it.
			// var deltaMS = delta * 1000;
			var deltaMS = (Date.now() - gameData.gameTimeTick);
			log('Drift: %d', deltaMS - delta * 1000);
			// log('%d %d, %d', gameData.gameTime, deltaMS, (Date.now() - gameData.gameTime));
			gameData.state = updateState(deltaMS);

			// Send the state down to the clients
			// gameData.gameTime = gameData.state.tick;
			gameData.gameTime += deltaMS;
			gameData.gameTimeTick = Date.now();
			if (gameData.state.winner) {
				gameloop.clearGameLoop(gameData.gameLoopId);
				io.to(gameData.gameId).emit(MDS.GAME_OVER, {
					gameId: gameId,
					winner: gameData.state.winner
				});
			} else if (gameData.state.hasToUpdateClients) {
				var data = common.packServerData({
					gameId: gameId,
					// gameTime: gameData.gameTime,
					gameTime: gameData.state.tick,
					state: gameData.state,
					input: {
						ship0: gameData.lastInput0,
						ship1: gameData.lastInput1
					}
				});
				io.to(gameId).emit(MDS.GAME_UPDATED, data);
				gameData.state.hasToUpdateClients = false;
			}

		}, SERVER_FRAME_MS);
		log('Started game loop %d', gameData.gameLoopId);

		// stop the loop DEBUG_MAX_TIME msecs later
		// TODO: We don't want the auto stop. We do want to stop if there was
		// no input for a while. Use an ERROR for that.
		setTimeout(function() {
			log(DEBUG_MAX_TIME + 'ms passed, stopping the game loop');
			gameloop.clearGameLoop(gameData.gameLoopId);
		}, DEBUG_MAX_TIME);
	}

	io.on('connection', function(client) {
		log('IO: Client ' + client.id + ' connected to Phaser');
		//client.userId = UUID();
		client.userId = 'server-id-' + ((Math.random() * 10000) | 0);
		client.emit('connected', {
			userId: client.userId
		});

		// Error handling
		function doError(data) {
			if (data.userId) {
				// This error is sent to a specific client
				logError('Client %s in game %d: %s.', data.userId, data.gameId, data.message);
				client.emit(MDS.GAME_ERROR, data);
			} else {
				// This error is sent to everyone in the room
				logError('Game %d: %s.', data.gameId, data.message);
				io.to(data.gameId).emit(MDS.GAME_ERROR, data);
			}
		}

		// Socket events
		client.on('disconnect', function() {
			log('IO: Client ' + client.userId + ' disconnected from Phaser');
			var gameLoopId = 0; // TODO: get it from the client id
			// TODO: check if it is possible to reconnect and maintain the state.
			// If this would be the case, then a lost connection that can be
			// recovered quickly might be fine. We can simulate it by disconnecting
			// a player for a couple of seconds.
			gameloop.clearGameLoop(gameLoopId);
		});

		// Server events
		client.on(MDC.CREATE_GAME, onCreate);
		client.on(MDC.JOIN_GAME, onJoin);
		client.on(MDC.CANCEL_GAME, onCancel);
		client.on(MDC.RESTART_GAME, onRestart);
		client.on(MDC.ON_INPUT, onInput);

		// Event handlers
		function onCreate(data) {
			log('IO: requestCreateGame by ' + data.userId);

			// Create a unique Socket.IO Room (its id is in gameID)

			// TODO: When using mongodb, the ID will be generated
			var gameId = ((Math.random() * 100000) | 0).toString();
			log('IO: Created Game ID: ' + gameId);
			client.join(gameId);
			log('IO: ' + data.userId + ' joined game ' + gameId);

			io.to(gameId).emit(MDS.GAME_CREATED, {
				userId: data.userId,
				gameId: gameId
			});
			log('IO: emit gameCreated by ' + data.userId + ' for game ' + gameId);

			if (gameData && gameData.gameLoopId) {
				gameloop.clearGameLoop(gameData.gameLoopId);
			}
			gameData = new GameData(gameId);
			gameData.player0 = data.userId;
		}

		function startGame(gameId) {
			// Send the 'ready' message to all players in the room.
			var state = common.initGameState();
			io.to(gameId).emit(MDS.GAME_READY, {
				gameId: gameId,
				state: state,
				countdown: COUNT_DOWN
			});
			gameData.setState(state);

			// Do the same countdown the clients do, and start
			setTimeout(function() {
				playGame(gameData);
			}, COUNT_DOWN * 1000);
		}

		function onJoin(data) {
			log('IO: requestJoinGame  ' + JSON.stringify(data));
			var gameId = data.gameId;
			if (!gameData || !gameData.gameId || gameData.gameId !== gameId) {
				// The game does not exist.
				doError({
					gameId: gameId,
					userId: data.userId,
					message: 'Cannot join game ' + gameId + '.<br/>Game does not exist.'
				});
				return;
			}
			client.join(gameId);
			log('IO: ' + data.userId + ' joined game ' + gameId);

			io.to(gameId).emit(MDS.GAME_JOINED, {
				userId: data.userId,
				gameId: gameId
			});
			log('IO: emit gameJoined by ' + data.userId + ' for game ' + gameId);
			gameData.player1 = data.userId;

			// Check that we have both players, then we can initialize and start
			if (gameData.player0 && gameData.player1) {
				// We have all players needed, let the games begin.
				startGame(gameId);
			}
		}

		function onCancel(data) {
			// This is called before the game started
			var gameId = data.gameId;
			if (gameData && gameData.gameId && gameData.gameId === gameId) {
				if (data.userId === gameData.player0) {
					// When the host cancels, the game is over
					log('Game %d terminated by host: %s', gameId, data.userId);
					io.to(gameId).emit(MDS.GAME_CANCELED, {
						userId: data.userId,
						gameId: gameId
					});
					if (gameData.gameLoopId) {
						gameloop.clearGameLoop(gameData.gameLoopId);
					}
					gameData = null;
				} else {
					// This is either the guest or a viewer
					log('Game %d left by guest: %s', gameId, data.userId);
					client.emit(MDS.GAME_CANCELED, {
						userId: data.userId,
						gameId: gameId
					});
				}
			} else {
				// The game doesn't exist, so doesn't matter...
				client.emit(MDS.GAME_CANCELED, {
					userId: data.userId,
					gameId: gameId
				});
			}
		}

		function onRestart(data) {
			// This is called after the game is over. We want to use a new
			// game ID with the same players.
			if (gameData && gameData.gameLoopId) {
				gameloop.clearGameLoop(gameData.gameLoopId);
			}
			var gameId = data.gameId;
			var newGameData = new GameData(gameData.gameId);
			newGameData.player0 = gameData.player0;
			newGameData.player1 = gameData.player1;
			gameData = newGameData;
			startGame(gameId);
		}

		function onInput(data) {
			// Queue the input, it will be handled during the game loop
			data = common.unpackClientData(data);
			// var dt = Date.now() - gameData.gameTimeTick;
			// data.input.gameTime = gameData.gameTime + dt;
			data.input.gameTime = gameData.gameTime;
			gameData.addInput(data.userId, data.input);
		}
	});
};

// Extra code for redis:

// if (0 < socket.rooms.indexOf(gameId)) {
// 	// This player is already in this room. Do nothing.
// 	console.log('User ' + username + ' is already in room ' + gameId);
// 	return;
// }

// User.where({
// 	username: username
// }).findOne().exec().then(function(user) {
// 	// Verify that the user is kosher for this specific game

// 	// Join the user to this room
// 	console.log('Joining user ' + username + ' to room ' + gameId);
// 	socket.join(gameId);

// 	// Update the game data
// 	redis.getAsync(gameId).then(function(_game) {
// 		console.log('Got game data: ' + _game);
// 		var game = JSON.parse(_game);

// 		// TODO: only add if not there yet; if there, update status
// 		game.players.push({
// 			username: username,
// 			id: user._id,
// 			status: 'joined'
// 		});
// 		redis.setAsync(gameId, JSON.stringify(game)).then(function(res) {
// 			console.log('Updated game data: ' + JSON.stringify(game));
// 			io.to(gameId).emit('gameJoined', {
// 				username: username,
// 				gameId: gameId
// 			});
// 			if (2 == game.players.length) {
// 				// We have all players needed, let the games begin.
// 				// Send the 'ready' message to all players in the room.
// 				console.log('Game ' + gameId + ' ready with players: ' + JSON.stringify(game.players));
// 				io.to(gameId).emit('gameReady', {
// 					gameId: gameId,
// 					players: game.players
// 				});
// 			}
// 		});
// 	});
// });

// // Store the game data in the DB (this should be a model later)
// var game = {
// 	created_at: new Date(),
// 	players: [],
// 	rounds: 5
// };
// redis.setAsync(gameId, JSON.stringify(game)).then(function() {
// 	// Return the details to the caller
// 	socket.emit('gameCreated', {
// 		gameId: gameId
// 	});

// 	// Set the game to expire in 24 hours (no worries about the
// 	// return value here)
// 	redis.expireAsync(gameId, 24 * 60 * 60);
// });