'use strict';

// Main dependencies and plugins
var gulp = require('gulp-help')(require('gulp'), {
	hideDepsMessage: true
});
var eslint = require('gulp-eslint');
// var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
// var rename = require('gulp-rename');
var sass = require('gulp-sass');
var nodemon = require('gulp-nodemon');

// To support hierarchy inside the folders, do 'assets/js/**/*.js'

// Our assets
var assetsJS = ['assets/js/*.js', 'assets/js/*.json', 'game/common.js'];
var assetsCSS = ['assets/css/*.scss', 'assets/css/*.css'];
var assetsIMG = ['assets/img/*.*'];
var assets = [assetsJS, assetsCSS, assetsIMG];

// Vendor assets
var vendorJS = 'assets/vendor/js/*.js';
var vendorCSS = ['assets/vendor/*.scss', 'assets/vendor/css/*.css'];

// Public assets
var publicJS = 'public/javascripts';
var publicCSS = 'public/stylesheets';
var publicIMG = 'public/images';

// Concatenate and minify all vendor JS files.
// This is not part of the on-going build - run it when there is a change.
gulp.task('vendor_js', false, function() {
	return gulp.src(vendorJS)
		// .pipe(concat('vendor.min.js'))
		// .pipe(uglify())
		.pipe(gulp.dest(publicJS));
});

gulp.task('vendor_css', false, function() {
	return gulp.src(vendorCSS)
		// .pipe(concat('vendor.min.scss'))
		// .pipe(sass({
		// 	outputStyle: 'compressed'
		// }).on('error', sass.logError))
		.pipe(gulp.dest(publicCSS));
});

gulp.task('vendor', false, ['vendor_js', 'vendor_css']);

// Lint Task
gulp.task('lint', 'Run the linter', function() {
	return gulp.src(assetsJS)
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

// Concatenate and minify all JS files
gulp.task('build_js', false, function() {
	return gulp.src(assetsJS)
		// DO NOT UGLIFY WHILE IN DEV
		//.pipe(concat('main.min.js'))
		//.pipe(uglify())
		.pipe(gulp.dest(publicJS));
});

// Concatenate, SASS and minify all SCSS files
gulp.task('build_css', false, function() {
	return gulp.src(assetsCSS)
		.pipe(concat('main.min.scss'))
		.pipe(sass({
			outputStyle: 'compressed'
		}).on('error', sass.logError))
		.pipe(gulp.dest(publicCSS));
});

// Move image files to the public area
gulp.task('build_img', false, function() {
	return gulp.src(assetsIMG)
		.pipe(gulp.dest(publicIMG));
});

var assetTasks = ['lint', 'build_js', 'build_css', 'build_img'];
gulp.task('assets', false, assetTasks);

// Watch asset files for changes
gulp.task('watch_assets', false, function() {
	gulp.watch(assets, assetTasks);
});

// Run the application under nodemon
gulp.task('demon', false, function() {
	nodemon({
		script: './bin/www',
		ext: 'js',
		watch: ['app.js', 'routes/', 'utils/', 'game/'],
		ignore: ['.git', 'bin/', 'node_modules/**/node_modules'],
		env: {
			'NODE_ENV': 'development'
		}
	})
	.on('restart', function() {
		console.log('**************************************');
		console.log('Application restarted by gulp-nodemon.');
		console.log('**************************************');
	});
});

// Default Task; it runs the app under nodemon (so it can restart on a source change).
// The assets task will rebuild assets when they change (then reload the browser).
gulp.task('default', 'Run the server and watch for changes', ['watch_assets', 'demon']);

// The refresh task is to be run after the environment was changed.
gulp.task('refresh', 'Refresh assets after a change', ['vendor', 'assets']);