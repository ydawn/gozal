var logger = require('../utils/logger');

module.exports = function(app) {

	// catch 404 and forward to error handler
	app.use(function(req, res, next) {
		var err = new Error('Not Found');
		err.status = 404;
		next(err);
	});

	app.use(function(err, req, res, next) {
		logger.error((new Date()).toLocaleTimeString() + ' execution error caught: ' + err);
		res.status(err.status || 500);
		var errMessage = {
			message: err.message,
		};
		if (app.get('env') === 'development') {
			// When not in dev, no stacktraces leaked to user
			errMessage.error = err;
		}
		res.format({
			html: function() {
				res.render('shared/error', errMessage);
			},

			json: function() {
				res.send(errMessage); // TODO: check if error status is returned
			}
		});
	});
};