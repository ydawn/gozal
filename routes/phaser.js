var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
	res.render('phaser/phaser', {
		title: 'Phaser Demo'
	});
});

module.exports = router;