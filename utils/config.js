var nconf = require('nconf');
var logger = require("../utils/logger");

module.exports = function() {

	// Setup nconf to use (in-order):
	//	1. Command-line arguments
	//	2. Environment variables
	//	3. Default config file
	//	4. Environment config file
	logger.info('Loading configuration from environment');
	nconf.argv().env();

	logger.info('Loading per environment configuration: ' + nconf.get('NODE_ENV'));
	nconf.file('environment', './config/' + nconf.get('NODE_ENV') + '.json');

	logger.info('Loading default configuration');
	nconf.file('default', './config/default.json');
};