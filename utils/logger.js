// var express = require('express');
// var app = express();

// The following levels are predefined:
// debug, verbose, info, warn, error
// For more: https://www.npmjs.com/package/winston
var winston = require('winston');
winston.emitErrs = true;

var logger = new winston.Logger({
	transports: [
		new winston.transports.File({
			level: 'info',
			filename: './log/all-logs.log',
			handleExceptions: true,
			json: true,
			maxsize: 5242880, // 5MB
			maxFiles: 5,
			colorize: false
		}),
		new winston.transports.Console({
			//level: (app.settings.env === 'development') ? 'info' : 'warn',
			level: 'warn',
			handleExceptions: true,
			json: false,
			colorize: true
		})
	],
	exitOnError: false
});

module.exports = logger;
module.exports.stream = {
	write: function(message, encoding) {
		logger.info(message);
	}
};