var logger = require('../utils/logger');

module.exports = function(app) {
	// This will be used in /bin/www and anywhere that needs the socket
	logger.info('Setting up socket.io');
	app.io = require('socket.io')();
};